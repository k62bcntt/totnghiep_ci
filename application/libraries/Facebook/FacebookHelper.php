<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	// namespace Facebook;
	require_once( APPPATH . 'libraries/Facebook/FacebookSession.php' );
	require_once( APPPATH . 'libraries/Facebook/FacebookRedirectLoginHelper.php' );
	require_once( APPPATH . 'libraries/Facebook/FacebookRequest.php' );
	require_once( APPPATH . 'libraries/Facebook/FacebookSDKException.php' );
	require_once( APPPATH . 'libraries/Facebook/Entities/AccessToken.php' );
	//-----------------viết đúng thứ tự 2 cái bên dưới này ----------------------------
	require_once( APPPATH . 'libraries/Facebook/HttpClients/FacebookHttpable.php' );
	require_once( APPPATH . 'libraries/Facebook/HttpClients/FacebookCurlHttpClient.php' );
	//------------------------------------------------------------------------------------
	require_once( APPPATH . 'libraries/Facebook/FacebookResponse.php' );
	require_once( APPPATH . 'libraries/Facebook/FacebookRequestException.php' );
	require_once( APPPATH . 'libraries/Facebook/FacebookAuthorizationException.php' );
	require_once( APPPATH . 'libraries/Facebook/GraphObject.php' );
	require_once( APPPATH . 'libraries/Facebook/HttpClients/FacebookCurl.php' );
	require_once( APPPATH . 'libraries/Facebook/FacebookServerException.php' );

	use Facebook\FacebookSession;
	use Facebook\FacebookRedirectLoginHelper;
	use Facebook\FacebookRequest;
	use Facebook\FacebookResponse;
	use Facebook\FacebookSDKException;
	use Facebook\FacebookRequestException;
	use Facebook\FacebookAuthorizationException;
	use Facebook\GraphObject;
	use Facebook\Entities\AccessToken;
	use Facebook\HttpClients\FacebookHttpable;
	use Facebook\HttpClients\FacebookCurlHttpClient;
	use Facebook\HttpClients\FacebookCurl; 
	use Facebook\FacebookServerException;

	class FacebookHelper{

		private $helper;
		private $fb;
		private $permission;
		private $session;
		private $fb_token;

		public function __construct(){
			//lấy ra các trường hợp
			if (session_status() == PHP_SESSION_NONE) 
			{
				session_start();
			}

			$this->fb =& get_instance();
			
			$this->permissions = $this->fb->config->item('permissions', 'facebook');
			FacebookSession::setDefaultApplication( 
				$this->fb->config->item('api_id', 'facebook'), 
				$this->fb->config->item('app_secret', 'facebook') 
			);

			$this->helper = new FacebookRedirectLoginHelper( 
				$this->fb->config->item('redirect_url', 'facebook')
			);
			
			try {
				  $this->session = $this->helper->getSessionFromRedirect();
			} catch(FacebookRequestException $ex) {
				  $this->session = null;
			} catch(Exception $ex) {
				  $this->session = null;
			}

			if ($this->session) {
				//lấy ra token rồi gán vào biến $this->session
      			$this->session = new FacebookSession( $this->session->getToken() );
      			//--------------------------------------------
      			$this->fb_token = $this->session->getAccessToken();
      			$this->fb_token = $this->fb_token->extend();
			}
			var_dump($this->helper->getSessionFromRedirect());
		}

		public function fb_image()
		{
			return "https://graph.facebook.com/".$this->fb->config->item('api_id', 'facebook')."/picture?width=100";
		}

		public function loginUrl(){
			return $this->helper->getLoginUrl();
		}

		public function logoutUrl($url_logout){
			$params = array('next'=>$url_logout);
			return $this->helper->getLogoutUrl($this->session,$params);
		}

		public function get_User(){
			if($this->session){
				$request = new FacebookRequest($this->session, 'GET', '/me');
				$response = $request->execute();
				$graphObject = $response->getGraphObject();

				return $graphObject->asArray();
			}
			return false;
		}

		public function getToken()
		{
			if($this->fb_token)
			{
				return $this->fb_token;
			}
			else
			{
				return false;
			}
		}

		// public function getSession(){
		// 	$session = $this->helper->getSessionFromRedirect();
		// 	if($session){
		// 		return $session;	
		// 	}
		// 	else{
		// 		return false;
		// 	}
		// }

		public function getSession(){
			if($this->session){
				return $this->session;	
			}
			else{
				return false;
			}
		}

		public function getFacebookAccessToken(){
			$longLivedAccessToken = new AccessToken($this->fb_token."");

			try {
			  // Get a code from a long-lived access token
			  $code = AccessToken::getCodeFromAccessToken($longLivedAccessToken);
			} catch(Exception $e) {
			  echo 'Error getting code: ' . $e->getMessage();
			  exit;
			}

			try {
			  // Get a new long-lived access token from the code
			  $newLongLivedAccessToken = AccessToken::getAccessTokenFromCode($code);
			} catch(Exception $e) {
			  echo 'Error getting a new long-lived access token: ' . $e->getMessage();
			  exit;
			}
			return $newLongLivedAccessToken."";
		}
	}
?>