﻿<footer>
  <div class="container">
    <div class="four columns">
      <div id="info">
        <h3>Thông tin</h3>
        <ul>
          <li><a href="<?php echo base_url().'gioi-thieu'; ?>">Về chúng tôi</a></li>
          <li><a href="<?php echo base_url().'lien-he'; ?>">Liên hệ</a></li>
        </ul>
      </div>
    </div><!--end three-->

    <div class="four columns">
      <div id="extra">
        <h3>Mở rộng</h3>
        <ul>
          <li><a href="#">Quà tặng </a></li>
          <li><a href="#">Hỗ trợ</a></li>
          <li><a href="#">Đặc biệt</a></li>
        </ul>
      </div>
    </div><!--end three-->

    <div class="four columns">
      <div id="my_account">
        <h3>Tài khoản</h3>
        <ul>
          <li><a href="<?php echo base_url().'dang-nhap-va-dang-ky'; ?>">Đăng nhập</a></li>
          <li><a href="<?php echo base_url().'dang-nhap-va-dang-ky'; ?>">Đăng ký</a></li>
          <li><a href="#">Bản tin</a></li>
        </ul>
      </div>
    </div><!--end three-->

    <div class="four columns">
      <div id="delivery" class="clearfix">
        <h3>Thông tin giao hàng</h3>
        <ul>
          <li class="f_call">Gọi chúng tôi: <?php echo $config['hotline']; ?></li>
          <?php if(isset($support) && !empty($support)):  ?>
            <?php foreach ($support as $key => $value): ?>
              <li class="f_mail">
              <?php echo $value['sup_name']; ?>: <br/>
              </li>
              <li><?php echo 'Phone: <a href="tel:'.$value['sup_phone'].'">'.$value['sup_phone'].'</a>'; ?> <br/></li>
              <li><?php echo 'Email: '.$value['sup_email']; ?> <br/></li>
              
            <?php endforeach; ?>
          <?php endif; ?>
        </ul>
      </div>
    </div><!--end four-->
  </div>
  <br/>
  <div class="container">
    <div class="sixteen">
      <p class="copyright">
        Bản quyền 2016 của <a href="<?php echo base_url(); ?>">ThoiTrangNu.com</a><br>
        Powered By: <a href="#">garung</a>
      </p>
      <ul class="socials">
        <li><a class="twitter" href="#">twitter</a></li>
        <li><a class="facebook" href="<?php echo $config['facebook']; ?>">face</a></li>
        <li><a class="googlep" href="#">google+</a></li>
        <li><a class="vimeo" href="#">vimeo</a></li>
        <li><a class="skype" href="#">skype</a></li>
        <li><a class="linked" href="#">linked</a></li>
      </ul>
    </div><!--end sixteen-->
  </div><!--end container-->
</footer>
<!--end the footer area -->