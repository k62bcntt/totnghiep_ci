<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/home/css/blog.css">
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
<div class="container">
	<div class="sixteen columns">
		<div id="pageName">
			<div class="name_tag">
				<p>
					Bạn đang ở đây :: <a href="#">Trang chủ</a> :: Blog
				</p>
				<div class="shapLeft"></div>
				<div class="shapRight"></div>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="eleven columns">
		<div class="col-xs-4">

        <ul class="nav nav-tabs tabs-left">
                <li class="active"><a href="#home" data-toggle="tab">Thông tin cá nhân</a></li>
                <li><a href="#profile" data-toggle="tab">Đổi mật khẩu</a></li>
                <li><a href="#messages" data-toggle="tab">Lịch sử mua sắm</a></li>
                <li><a href="#settings" data-toggle="tab">Tin nhắn</a></li>
            </ul>
        </div>

        <div class="col-xs-8">
           <div class="tab-content">
                <div class="tab-pane active" id="home">
                	<div class="row">
                		<div class="col-md-3">
                			<img src="" width="100%" height="100%">
                		</div>
                		<div class="col-md-9">
                			<div class="form-group">
							    <label for="usr">Họ tên:</label>
							    <input type="text" class="form-control" id="usr">
							</div>
							<div class="form-group">
							    <label for="pwd">E-mail:</label>
							    <input type="text" class="form-control" id="pwd">
							</div>
							<div class="form-group">
							    <label for="pwd">Ngày sinh:</label>
							    <input type="password" class="form-control" id="pwd">
							</div>
							<div class="form-group">
							    <label for="usr">Số điên thoại:</label>
							    <input type="text" class="form-control" id="usr">
							</div>
							<div class="form-group">
							    <label for="pwd">Nơi công tác:</label>
							    <input type="password" class="form-control" id="pwd">
							</div>
							<div class="form-group">
							    <label for="usr">Quê quán:</label>
							    <input type="text" class="form-control" id="usr">
							</div>
                		</div>
                	</div>
                </div>
                <div class="tab-pane" id="profile">
                	<div class="form-group">
					    <label for="usr">Mật khẩu cũ:</label>
					    <input type="password" class="form-control" id="usr">
					</div>
					<div class="form-group">
					    <label for="pwd">Mật khẩu mới:</label>
					    <input type="password" class="form-control" id="pwd">
					</div>
					<div class="form-group">
					    <label for="pwd">Xác nhận mật khẩu:</label>
					    <input type="password" class="form-control" id="pwd">
					</div>
                </div>
                <div class="tab-pane" id="messages">Chưa có lịch sử mua sắm</div>
                <div class="tab-pane" id="settings">Chưa có tin nhắn</div>
            </div>
        </div>
	</div>

	<aside class="five columns">
		<div class="blogSearch clearfix">
			<form method="get" action="#">
				<label>
					<input class="input_tool_tip" type="text" name="search" placeholder="Search In Blog">
				</label>
				<div class="submit">
					<input type="submit" name="submit">
				</div>
			</form>
		</div><!--end blogSearch-->

		<div class="blog_category clearfix">
			<div class="box_head">
				<h3>Category</h3>
			</div><!--end box_head -->
			<ul>
				<li><a href="#">Áo đơn</a></li>
				<li><a href="#">Quần đơn</a></li>
				<li><a href="#">Áo đôi</a></li>
				<li><a href="#">Đồng bộ</a></li>
			</ul>
			<ul>
				<li><a href="#">Giày</a></li>
				<li><a href="#">Mũ</a></li>
				<li><a href="#">Túi xách</a></li>
				<li><a href="#">Ví</a></li>
			</ul>
		</div><!--end blogArchive-->

		<div class="blogTab">
			<div id="tabs">
				<ul class="tabNav">
					<li><a class="currentTab" href="#recent">Mới</a></li>
					<li><a href="#puplar">Hot</a></li>
					<li><a href="#comment">Comment</a></li>
					<div class="clear"></div>
				</ul>
				<div id="tabContentOuter">
					<div id="recent" class="tabContent">
						<div>
							<img src="images/photos/thumbnail.jpg" alt="">
							<div>
								<a href="#">
								<p>Sản phẩm mới 1</p>
								</a>
								<span>16 October, 2012</span>
							</div>
						</div>

						<div>
							<img src="images/photos/thumbnail.jpg" alt="">
							<div>
								<a href="#">
								<p>Sản phẩm mới 1</p>
								</a>
								<span>16 October, 2012</span>
							</div>
						</div>

						<div>
							<img src="images/photos/thumbnail.jpg" alt="">
							<div>
								<a href="#">
								<p>Sản phẩm mới 1</p>
								</a>
								<span>16 October, 2012</span>
							</div>
						</div>
						<div class="clear"></div>
					</div><!--end recent-->


					<div id="puplar" class="tabContent">
						<div>
							<img src="images/photos/thumbnail.jpg" alt="">
							<div>
								<a href="#">
								<p>Sản phẩm hot 1</p>
								</a>
								<span>8 September, 2012</span>
							</div>
						</div>

						<div>
							<img src="images/photos/thumbnail.jpg" alt="">
							<div>
								<a href="#">
								<p>Sản phẩm hot 1</p>
								</a>
								<span>18 September, 2012</span>
							</div>
						</div>

						<div>
							<img src="images/photos/thumbnail.jpg" alt="">
							<div>
								<a href="#">
								<p>Sản phẩm hot 1</p>
								</a>
								<span>16 September, 2012</span>
							</div>
						</div>
						<div class="clear"></div>
					</div><!--end puplar-->


					<div id="comment" class="tabContent">
						<div>
							<img src="images/photos/thumbnail.jpg" alt="">
							<div>
								<a href="#">
								<p>Bình luận 1</p>
								</a>
								<span>15 May, 2012</span>
							</div>
						</div>

						<div>
							<img src="images/photos/thumbnail.jpg" alt="">
							<div>
								<a href="#">
								<p>Bình luận 2 </p>
								</a>
								<span>9 Januury, 2012</span>
							</div>
						</div>

						<div>
							<img src="images/photos/thumbnail.jpg" alt="">
							<div>
								<a href="#">
								<p>Bình luận 3</p>
								</a>
								<span>11 June, 2012</span>
							</div>
						</div>
						<div class="clear"></div>
					</div><!--end comment-->
				</div><!--end tabContentOuter-->
			</div><!--end tabs-->
		</div><!--end blogTab-->

		<div class="ads">
			<h6>Quảng cáo</h6>
		</div><!--end ads-->

	</aside><!--end aside five-->


</div><!--end container-->