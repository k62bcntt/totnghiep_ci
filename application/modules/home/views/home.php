<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/home/css/home.css">
<div class="container">

	<div class="ten columns">
		<div class="welcome">
			<div class="clearfix">
				<h2>Chào mừng bạn đến với Thời trang phụ nữ</h2>
				<p>
					Chúng tôi luôn mang đến cho phụ nữ những sản phẩm thời trang hợp với phong cách của nhiều người. Những sản phẩm được thiết kế trang nhã, hợp thời trang, quyến rũ mà không làm mất đi thuần phong mỹ tục, mang lại cho bạn cảm giác tuyệt vời nhất.
				</p>
				<p>
					Cùng với những phương thức thanh toán đơn giản mà nhanh chóng, bạn sẽ có ngay những sản phẩm thời trang mà bạn yêu thích
				</p>
				<h4>Dưới đây là một trong những phương thức thanh toán:</h4>
				<ul>
					<li><a class="bank" href="#">text</a></li>
					<li><a class="card" href="#">text</a></li>
					<li><a class="order" href="#">text</a></li>
					<li><a class="paypal" href="#">text</a></li>
					<li><a class="discover" href="#">text</a></li>
				</ul>
			</div>
		</div><!--end welcome-->
	</div><!--end ten-->

	<div class="six columns">
		<div class="home_news">
			<h3>Nào ! Hãy đến với những sản phẩm giảm đến 50%</h3>
			<div class="acc">
				<p>Thời Trang Nữ của chúng tôi luôn có những mặt hàng giảm giá lên đến 50%, như vậy các chị em có thể có được những sản phẩm mình thích mà không phải bỏ ra quá nhiều tiền !</p>
			</div>
		
			<h3>Tuỳ chọn nhiều mặt hàng</h3>
			<div class="acc">
				<p>
					Đến với gian hàng của chúng tôi, các bạn có thể tuỳ chọn các sản phẩm thời trang hợp với mình nhất
				</p>
			</div>

			<h3>Hãy đăng ký nhận thông tin mới nhất</h3>
			<div class="acc">
				<p>
					Chúng tôi sẽ gửi tới các bạn những thông tin mới nhất về sản phẩm, những chương trình giảm giá, các mặt hàng giảm giá. Tất cả thông tin chúng tôi ssex gửi qua e-mail của bạn.
				</p>
				<form method="post" action="<?php echo base_url().'send-mail'; ?>" class="clearfix">
					<label>
						<input type="text" name="newslatter" placeholder="Nhập E-mail của bạn" value="">
					</label>
					<label>
						<input class="gray_btn" type="submit" name="submit" value="Theo dõi">
					</label>
				</form>
			</div>

			<h3>Bây giờ, hãy cổ vũ tinh thần cho chúng tôi</h3>
			<div class="acc">
				<p>
					Chúng tôi rất cần sự cổ vũ, sự ủng hộ đến từ mọi người để có thể đem đến cho tất cả chị em những chương trình giảm giá các sản phẩm thời trang. Và đó có thể coi là cách đền đáp các khách hàng thân thiết
				</p>
			</div>
		</div><!--end home_news-->
	</div><!--end six-->


	<div class="eight columns">
		<div class="latest">
			
			<div class="box_head">
				<h3>Sản phẩm hot</h3>
				<div class="pagers center">
					<a class="prev latest_prev" href="#prev">Prev</a>
					<a class="nxt latest_nxt" href="#nxt">Next</a>
				</div>
			</div><!--end box_head -->

			<div class="cycle-slideshow" 
	        data-cycle-fx="scrollHorz"
	        data-cycle-timeout=0
	        data-cycle-slides="> ul"
	        data-cycle-prev="div.pagers a.latest_prev"
	        data-cycle-next="div.pagers a.latest_nxt"
	        >
	        <?php if(isset($pro_hot)  && !empty($pro_hot)): ?>

				<?php 
				$dem = count($pro_hot);
				$start = 0;
				$end = 4;
				while($end <= $dem){
					echo '<ul class="product_show">';
					for($i = $start; $i < $end; $i++)
					{
						?>
						<li>
							<div class="img">
								<div class="hover_over">
									<a class="link" href="<?php echo base_url().'chi-tiet-san-pham/'.$pro_hot[$i]['pro_name_rewrite'].'-'.$pro_hot[$i]['pro_id']; ?>">link</a>
									<a class="cart" href="<?php echo base_url().'them-gio-hang/'.$pro_hot[$i]['pro_id']; ?>">cart</a>
								</div>
								<a href="#">
									<img src="<?php echo $pro_hot[$i]['pro_images']; ?>" alt="<?php echo (isset($pro_hot[$i]['pro_name']) ? $pro_hot[$i]['pro_name'] : 'product'); ?>" width="100%" height="100%">
								</a>
							</div>
							<h6><a href="#"><?php echo (isset($pro_hot[$i]['pro_name']) ? $pro_hot[$i]['pro_name'] : ''); ?></a></h6>
							<h5><?php echo (isset($pro_hot[$i]['pro_price']) ? $pro_hot[$i]['pro_price'] : ''); ?> VND</h5>
						</li>
						<?php
					}
					echo '</ul>';
					$start = $start + 4;
					$end = $end + 4;
					if($end > $dem)
					{
						break;
					}
				}
				
				if(($dem-$end) != 0)
				{
					echo '<ul class="product_show">';
					for($i = $start; $i < $dem; $i++)
					{
						?>
						<li>
							<div class="img">
								<div class="hover_over">
									<a class="link" href="<?php echo base_url().'chi-tiet-san-pham/'.$pro_hot[$i]['pro_name_rewrite'].'-'.$pro_hot[$i]['pro_id']; ?>">link</a>
									<a class="cart" href="<?php echo base_url().'them-gio-hang/'.$pro_hot[$i]['pro_id']; ?>">cart</a>
								</div>
								<a href="<?php echo base_url().'chi-tiet-san-pham/'.$pro_hot[$i]['pro_name_rewrite'].'-'.$pro_hot[$i]['pro_id']; ?>">
									<img src="<?php echo $pro_hot[$i]['pro_images']; ?>" width="100%" height="100%" alt="<?php echo (isset($pro_hot[$i]['pro_name']) ? $pro_hot[$i]['pro_name'] : 'product'); ?>">
								</a>
							</div>
							<h6><a href="<?php echo base_url().'chi-tiet-san-pham/'.$pro_hot[$i]['pro_name_rewrite'].'-'.$pro_hot[$i]['pro_id']; ?>"><?php echo (isset($pro_hot[$i]['pro_name']) ? $pro_hot[$i]['pro_name'] : ''); ?></a></h6>
							<h5><?php echo (isset($pro_hot[$i]['pro_price']) ? $pro_hot[$i]['pro_price'] : ''); ?> VND</h5>
						</li>
						<?php
					}
					echo '</ul>';
				}
				?>
			<?php endif; ?>
			<a href="<?php echo base_url().'danh-muc/'; ?>" class="red_btn" >Xem nhiều hơn ...</a>
			</div>
		</div>
	</div>

	<div class="eight columns">
		<div class="featured">
		<?php if(isset($pro_sale)  && !empty($pro_sale)): ?>
			<div class="box_head">
			<h3>Sản phẩm mua nhiều</h3>
				<div class="pagers center">
					<a class="prev featuredPrev" href="#prev">Prev</a>
					<a class="nxt featuredNxt" href="#nxt">Next</a>
				</div>
			</div><!--end box_head -->

			<div class="cycle-slideshow" 
	        data-cycle-fx="scrollHorz"
	        data-cycle-timeout=0
	        data-cycle-slides="> ul"
	        data-cycle-prev="div.pagers a.featuredPrev"
	        data-cycle-next="div.pagers a.featuredNxt"
	        >
	        <?php 
				$dem = count($pro_sale);
				$start = 0;
				$end = 4;
				while($end <= $dem){
					echo '<ul class="product_show">';
					for($i = $start; $i < $end; $i++)
					{
						?>
						<li>
							<div class="img">
								<div class="hover_over">
									<a class="link" href="<?php echo base_url().'chi-tiet-san-pham/'.$pro_sale[$i]['pro_name_rewrite'].'-'.$pro_sale[$i]['pro_id']; ?>">link</a>
									<a class="cart" href="<?php echo base_url().'them-gio-hang/'.$pro_sale[$i]['pro_id']; ?>">cart</a>
								</div>
								<a href="#">
									<img src="<?php echo $pro_sale[$i]['pro_images']; ?>" alt="<?php echo (isset($pro_sale[$i]['pro_name']) ? $pro_sale[$i]['pro_name'] : 'product'); ?>" width="100%" height="100%">
								</a>
							</div>
							<h6><a href="<?php echo base_url().'chi-tiet-san-pham/'.$pro_sale[$i]['pro_name_rewrite'].'-'.$pro_sale[$i]['pro_id']; ?>"><?php echo (isset($pro_sale[$i]['pro_name']) ? $pro_sale[$i]['pro_name'] : ''); ?></a></h6>
							<h5><?php echo (isset($pro_sale[$i]['pro_price']) ? $pro_sale[$i]['pro_price'] : ''); ?> VND</h5>
						</li>
						<?php
					}
					echo '</ul>';

					$start = $start + 4;
					$end = $end + 4;
					if($end > $dem)
					{
						break;
					}
				}
				if(($dem - $end) != 0)
				{
					echo '<ul class="product_show">';
					for($i = $start; $i < $dem; $i++)
					{
						?>
						<li>
							<div class="img">
								<div class="hover_over">
									<a class="link" href="<?php echo base_url().'chi-tiet-san-pham/'.$pro_sale[$i]['pro_name_rewrite'].'-'.$pro_sale[$i]['pro_id']; ?>">link</a>
									<a class="cart" href="<?php echo base_url().'them-gio-hang/'.$pro_sale[$i]['pro_id']; ?>">cart</a>
								</div>
								<a href="#">
									<img src="<?php echo $pro_sale[$i]['pro_images']; ?>" alt="<?php echo (isset($pro_sale[$i]['pro_name']) ? $pro_sale[$i]['pro_name'] : 'product'); ?>" width="100%" height="100%">
								</a>
							</div>
							<h6><a href="#"><?php echo (isset($pro_sale[$i]['pro_name']) ? $pro_sale[$i]['pro_name'] : ''); ?></a></h6>
							<h5><?php echo (isset($pro_sale[$i]['pro_price']) ? $pro_sale[$i]['pro_price'] : ''); ?> VND</h5>
						</li>
						<?php
					}
					echo '</ul>';
				}
				?>
			</div>
		<?php endif; ?>
		</div>
	</div>

	<div class="sixteen columns">
		<section id="tagLine" class="clearfix">
			<div class="twelve columns">
				<h5>
					Thời Trang Nữ <span>Hãy khám phá tất cả sản phẩm của chúng tôi từ đầu.</span><br>
					<small>Chúc bạn có một ngày mua sắm vui vẻ</small>
				</h5>
			</div>
			<div class="three columns">
				<a class="red_btn" href="<?php echo base_url().'danh-muc'; ?>">Khám phá</a>
			</div>
			
		</section>
	</div>
</div>

<?php 

if(isset($_GET['addcart']))
{
	$id = $_GET['addcart'];
}

?>