<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/home/css/cart.css">
<div class="container">
    <div class="sixteen columns">
        <div id="pageName">
            <div class="name_tag">
                <p>
                    Bạn đang ở :: <a href="#">Trang chủ</a> :: Giỏ hàng của bạn
                </p>
                <div class="shapLeft"></div>
                <div class="shapRight"></div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="sixteen columns">     
        <div class="box_head">
            <h3>Giỏ hàng của bạn</h3>
        </div>

        <table class="cart_table">
            <thead>
                <tr>
                    <th class="first_td"><h4>Sản phẩm</h4></th>
                    <th><h4>Số lượng</h4></th>
                    <th><h4>Giá sản phẩm</h4></th>
                    <th><h4>Tổng tiền</h4></th>
                    <th><h4>Xoá</h4></th>
                </tr>
            </thead>
            <tbody>
                <?php $tongtien2 = 0; if(isset($_SESSION['cart'])  && is_array($_SESSION['cart'])): ?>
                    <?php foreach ($_SESSION['cart'] as $key => $value): ?>
                    <tr>
                        <td class="first_td">
                            <div class="clearfix">
                                <img src="<?php echo base_url().$value['image']; ?>" alt="product image" width="120px" height="100px">
                                <span>
                                    <strong><a href="#"><?php echo $value['name']; ?></a></strong><br>
                                    <ul>
                                        <li><a href="javascript:void(0);">text</a></li>
                                        <li><a href="javascript:void(0);">text</a></li>
                                        <li><a href="javascript:void(0);">text</a></li>
                                        <li><a href="javascript:void(0);">text</a></li>
                                        <li><a href="javascript:void(0);">text</a></li>
                                    </ul>
                                </span>
                            </div>
                        </td>
                        <td class="quantity">
                            <form action="gio-hang/cap-nhat" method="post" id="update_qty_<?php echo $value['id']; ?>">
                            <label>
                                <input class="gray_btn min_qty" type="button" stt="<?php echo $value['id']; ?>" value="-">
                            </label>

                            <label>
                                <input type="text" maxlength="2" name="qty" class="val_qty" id="val_qty_<?php echo $value['id']; ?>" stt=<?php echo $value['id']; ?> class="" value="<?php  echo $value['qty']; ?>" size="2">
                            </label>

                            <input type="hidden" name="pro_id" id="pro_id" class="" value="<?php  echo $value['id']; ?>">

                            <label>
                                <input class="gray_btn max_qty" type="button" stt="<?php echo $value['id']; ?>" value="+">
                            </label>
                            </form>
                            <button class="gray_btn" form="update_qty_<?php echo $value['id']; ?>" type="submit"><span>Sửa số lượng</span></button>
                        </td>
                        <td>
                            <h5><?php echo $value['price']; ?><span> VNĐ</span></h5>
                        </td>
                        <td class="total_price">
                            <h5>
                            <?php 
                                $tong = 0; 
                                $tong = ($value['price']*$value['qty']);
                                echo $tong; 
                                $tongtien2 += $tong; 
                            ?>
                            <span>VNĐ</span></h5>
                        </td>
                        <td>
                            <!-- class="delete_item" -->
                            <a class="" href="<?php echo base_url().'gio-hang/xoa-san-pham/'.$value['id']; ?>">Xóa</a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
                
            </tbody>
        </table><!--end cart_table-->
    </div><!--end sixteen-->

    <div class="ten columns">
        <div class="cart_tabs clearfix">

            <ul class="cart_tabs_nav">
                <li><a class="active_tab" href="#estimate">Vận chuyển và giao hàng</a></li>
                <li><a href="#discount_code">Nhập mã code</a></li>
                <li><a href="#gift_voucher">Sử dụng thẻ Voucher</a></li>
            </ul><!--end cart_tabs_nav-->

            <div class="cart_tabs_content">
                <div id="estimate" class="tab">
                    <p>Nhập nơi nhận sản phẩm</p>
                    <form method="#" action="#">
                        <label class="clearfix">
                        <strong>Nơi nhận sản phẩm của bạn:</strong>
                        <select class="default" tabindex="1">
                            <option value="">-- Nhập địa chỉ --</option>
                            <option value="sometext1">sometext1</option>
                            <option value="sometext2">sometext2</option>
                            <option value="sometext3">sometext3</option>
                        </select>
                        </label>

                        <label class="clearfix">
                            <strong>Vùng miền:</strong>
                            <select name="state" class="default" tabindex="1">
                                <option value="">-- Chọn vùng miền --</option>
                                <option value="text1">text1</option>
                                <option value="text2">text2</option>
                                <option value="text3">text3</option>
                            </select>
                        </label>

                        <div class="submit">
                            <input class="gray_btn" type="submit" name="submit" value="Nhận báo giá">
                        </div>
                    </form>
                </div>


                <div id="discount_code" class="tab">
                    <p>Nhập mã code khuyến mại vào đây...</p>
                    <form method="#" action="#">
                        <label for="country" class="clearfix">
                            <strong>Mã Code:</strong>
                            <input type="text" name="post_code" value="" placeholder="Enter Code">
                        </label>

                        <div class="submit">
                            <input class="gray_btn" type="submit" name="submit" value="Apply Coupon">
                        </div>
                    </form>
                </div>

                <div id="gift_voucher" class="tab">
                    <p>Nhập mã thẻ voucher ở đây...</p>
                    <form method="#" action="#">
                        <label for="country" class="clearfix">
                            <strong>Mã voucher:</strong>
                            <input type="text" name="post_code" value="" placeholder="Enter Code">
                        </label>

                        <div class="submit">
                            <input class="gray_btn" type="submit" name="submit" value="Áp dụng voucher">
                        </div>
                    </form>
                </div>
            </div><!--end cart_tabs_content-->

        </div><!--end cart_tabs-->
    </div><!--end ten-->

    <div class="six columns">
        <table class="receipt">
            <tbody>
                <tr>
                    <td>Tiền chưa thuế</td>
                    <td><?php echo $tongtien2.' VNĐ'; ?></td>
                </tr>
                <tr>
                    <td>Giá cước</td>
                    <td>0 VNĐ</td>
                </tr>
                <tr>
                    <td>Giảm giá</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td>Thuế VAT</td>
                    <td>10% (đã cộng)</td>
                </tr>

                <tr>
                    <td style="font-weight:600;font-size:16px;">Tổng tiền</td>
                    <td style="font-weight:600;font-size:16px;"><?php echo $tongtien2 + ($tongtien2*0.1).' VNĐ'; ?></td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td class="last_td">
                        <a class="gray_btn" href="<?php echo base_url(); ?>">Tiếp tục mua sắm</a>
                    </td>
                    <td class="last_td">
                        <a class="red_btn" href="<?php echo base_url().'don-hang'; ?>">Checkout</a>
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
