<?php 
    function dequymenu($menus,$id_parent)
    {
      echo '<li>';
      $loop = array();
      $name_parent = '';
      foreach ($menus as $key => $value) {
        if($value['cate_id_parent'] == $id_parent)
        {
          $loop[] = $value;
          $name_parent = $value['cate_name'];
        }
      }
      if($name_parent == '')
      {
          echo '<a class="tablets" href="#">'.$menus[0]['cate_name'].'</a>';
      }
      else
      {
        echo '<a class="hasdropdown" href="#">'.$name_parent.'</a>';
        echo '<ul class="submenu">';
        foreach ($loop as $key => $menu) {
          echo '<li><a href="#">'.$menu['cate_name'].'</a></li>';
          dequymenu($menus,$menu['cate_id']);
        }
        echo '</ul>';
      }
      echo '</li>';
    }
?>
<?php $count = isset($_SESSION['cart']) ? count($_SESSION['cart']) : 0; ?>
<header>
  <div id="topHeader">
    <div class="container">
      <div class="sixteen columns">         
        <ul id="topNav">
        <?php if(isset($_SESSION['name']) && !empty($_SESSION['name'])): ?>
          <li><a href="#">Xin chào, <?= $_SESSION['name']; ?></a></li>
          <li><a href="<?php echo base_url().'sua-thong-tin'; ?>">Thông tin cá nhân</a></li>
        <?php else: ?>
          <li><a href="<?php echo base_url().'dang-ky-va-dang-nhap'; ?>">Đăng ký và Đăng nhập</a></li>
        <?php endif; ?>
          <li><a href="<?php echo base_url().'gio-hang'; ?>">Giỏ hàng (<?= $count; ?>)</a></li>
          <li><a href="<?php echo base_url().'lien-he'; ?>">Liên hệ</a></li>
          <?php if(isset($_SESSION['id']) && !empty($_SESSION['id'])): ?>
            <li><a href="<?php echo base_url().'thoat'; ?>">Đăng xuất</a></li>
          <?php endif; ?>
        </ul>
      </div>
    </div>
  </div>

  <div id="middleHeader">
    <div class="container">
      <div class="sixteen columns">
        <div id="logo">
          <a href="<?php echo base_url(); ?>" style="background: rgba(0, 0, 0, 0) url('<?php echo (!empty($config['logo']) ? base_url().$config['logo'] : '../public/images/no-images.jpg') ?>') no-repeat scroll left top; display: block; font: 0px/0 a; height: 46px; width: 167px;">logo</a>
        </div><!--end logo-->

        <form action="<?php echo base_url(); ?>tim-kiem" method="get" accept-charset="utf-8">
          <label>
            <input type="text" name="s" placeholder="Tìm kiếm theo tên sản phẩm" value="">
          </label>
          <div class="submit">
            <input type="submit" id="searchsubmit">
          </div>
        </form>

      </div>
    </div>
  </div>

  <div class="container">
    <div class="sixteen columns">
      <div id="mainNav" class="clearfix">
        <nav>
          <ul>
            <li><a class="tablets" href="<?php echo base_url(); ?>">Trang chủ</a></li>
            <li><a class="labtops" href="<?php echo base_url().'gioi-thieu'; ?>">Giới thiệu</a></li>
            <li><a class="labtops" href="<?php echo base_url().'tin-tuc'; ?>">Tin tức</a></li>
            <?php if(isset($menus) && !empty($menus)): ?>
              <?php
              // dequymenu($menus,0);
              ?>
            <?php endif; ?>
            <li><a class="labtops" href="<?php echo base_url().'danh-muc'; ?>">Danh mục sản phẩm</a></li>
            <li><a class="tablets" href="<?php echo base_url().'lien-he'; ?>">Liên hệ</a></li>
          </ul>

        </nav><!--end nav-->
        
        <div id="cart">
          <a class="cart_dropdown" href="javascript:void(0);"><img src="<?php echo base_url(); ?>public/home/images/icons/cart_icon.png" alt="cart_icon">
          <?php  
            $tongtien = 0;
            $vat = 0;
            $total_price = 0;
            if(isset($_SESSION['cart'])  && is_array($_SESSION['cart']) && !empty($_SESSION['cart'])){
              foreach ($_SESSION['cart'] as $key => $value) {
                $total_price += ($value['price']*$value['qty']);
              }
            }
            $tongthanhtoan = $total_price + ($total_price*0.1);
            echo '<h3 style="display:inline;">'.$count.' Sản phẩm: '.$tongthanhtoan.' VND </h3>';
          ?>
          </a>
          <div class="cart_content">
            <b class="cart_content_arrow"></b>
            <ul>
              
              <?php if(isset($_SESSION['cart'])  && is_array($_SESSION['cart'])): ?>
                <?php 
                $dem = 1;
                foreach ($_SESSION['cart'] as $key => $value): 
                  $tongtien += $value['price'] * $value['qty'];
                ?>
                  <li class="clearfix">
                    <div class="cart_product_name">
                        <div class="container">
                          <div class="third columns">
                            <img src="<?= base_url().$value['image']; ?>" alt="product image" width="120px" height="100px">
                          </div>
                          <div class="six columns">
                            <span>
                              <strong><a href="#"><?= $value['name']; ?></a></strong><br>
                            </span>
                            <br />
                            <div class="">
                              <span>
                                <h4 style="display: inline;"><strong><?= $value['qty']; ?> x - <?= $value['price'].' VND'; ?></strong></h4><br>
                                <a class="" href="<?php echo base_url().'gio-hang/xoa-san-pham/'.$value['id']; ?>">Remove</a>
                              </span>
                            </div>
                          </div> 
                        </div>
                    </div>
                    <div class="clear"></div>
                  </li>
                  <?php if($dem == 2) {break;} $dem++; ?>
                <?php endforeach; ?>
              <?php endif; ?>              
            </ul>

            <div class="dropdown_cart_info clearfix">
              <div class="cart_buttons">
                <a class="gray_btn" href="<?php echo base_url().'gio-hang'; ?>">Xem giỏ hàng</a><br>
                <a class="red_btn" href="<?php echo base_url().'don-hang'; ?>">Checkout</a>
              </div>

              <div class="cart_total_price">
                <span>
                  Tổng tiền : <?= $total_price.' VND'; ?><br>
                  VAT 10% : <?php $vat = (int)($total_price*0.1); echo $vat.' VND' ?><br>
                  <strong>TỔNG CỘNG : <?php echo ($tongthanhtoan). ' VND'; ?></strong><br/>
                  <hr/>
                  <a href="<?php echo base_url().'gio-hang/xoa-tat-ca-san-pham' ?>">Hủy tất cả đã chọn</a>
                </span>
              </div>
            </div>

          </div>
        </div>

      </div><!--end main-->
    </div><!--end sixteen-->
  </div><!--end container-->
</header>