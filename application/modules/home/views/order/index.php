<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/home/css/checkout.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/home/css/wish_list.css">
<div class="container">
  <div class="sixteen columns">
    <div id="pageName">
      <div class="name_tag">
        <p>
          Bạn đang ở :: <a href="<?php echo base_url(); ?>">Trang chủ</a> :: Đơn hàng
        </p>
        <div class="shapLeft"></div>
        <div class="shapRight"></div>
      </div>
    </div>

  </div>
</div><!-- container -->
<div class="container">
  <div class="sixteen columns">
    <?php  
      if(isset($statusOrder))
      {
          if($statusOrder)
          {
            ?>
            <div class="alert alert-success">
              <h2 style="color: #1799A3"><strong>Thành công!</strong> Đơn hàng đã được gửi đi - Hãy chờ chúng tôi xác nhận lại trong thời gian tới.</h2><br /> Hãy tiếp tục mua sắm những sản phẩm khác của chúng tôi đi nào !
            </div>
            <?php
          }
          else
          {
            ?>
            <div class="alert alert-danger">
              <strong>Thất bại!</strong> Đơn hàng không được gửi đi - Hãy kiểm tra lại thông tin nhận hàng.
            </div>
            <?php
          }
      }
    ?>
  </div>
</div>
<div class="container">
  <div class="sixteen columns">
    <div class="box_head">
      <h3>Checkout</h3>
    </div><!--end box_head -->
  <form action="" method="post">
    <div class="checkout_outer">
      <h2>Bước 1 : <span>Kiểm tra lại sản phẩm đã chọn</span></h2>
      <div class="checkout_content checkout_option clearfix">
        <div class="sixteen columns">
          <table class="cart_table">
            <thead>
              <tr>
                <th class="first_td"><h4>Sản phẩm</h4></th>
                <th><h4>Giá</h4></th>
                <th><h4>Số lượng</h4></th>
                <th><h4>Xóa sản phẩm</h4></th>
              </tr>
            </thead>
            <tbody>
              <?php $tongtien2 = 0; if(isset($_SESSION['cart'])  && is_array($_SESSION['cart'])): ?>
                <?php foreach ($_SESSION['cart'] as $key => $value): ?>
                  <tr>
                    <td class="first_td">
                      <div class="clearfix">
                        <img src="<?php echo base_url().$value['image']; ?>" alt="<?php echo $value['name']; ?>" width="120px" height="100px">
                        <span>
                          <strong><br/><br/><?php echo $value['name']; ?></strong><br>
                        </span>
                      </div>
                    </td>
                    <td>
                      <h5><?php  echo $value['price'].' VNĐ'; ?></h5>
                    </td>
                    <td>
                      <?php  echo $value['qty']; ?>
                    </td>
                    <td>
                      <a class="" href="<?php echo base_url().'gio-hang/xoa-san-pham/'.$value['id']; ?>">Xóa</a>
                    </td>
                  </tr>
                  <?php 
                      $tong = 0; 
                      $tong = ($value['price']*$value['qty']);
                      $tongtien2 += $tong; 
                  ?>
                <?php endforeach; ?>
              <?php endif; ?>
                  <tr>
                    <td colspan="2"><h2>Tổng tiền ( Đã bao gồm 10% VAT) : </h2></td>
                    <td colspan="2"><h2><?php echo '<span style="color:#E81962">'.($tongtien2+($tongtien2*0.1)).'</span> VNĐ'; ?></h2></td>
                  </tr>
                  <tr>
                    <td colspan="3"></td>
                    <td><button class="red_btn" disabled="disabled">Tiếp tục bước 2</button></td>
                  </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  
    <div class="checkout_outer">
      <h2>Bước 2 : <span>Thông tin nhận hàng hóa </span></h2>
      <div class="checkout_content billing_account clearfix">
        <table>
          <tr>
            <td>
              <strong>Họ và tên đầy đủ của bạn <em>*</em></strong>
            </td>
            <td class="input">
              <input type="text" name="con_name" value="<?php echo set_value('con_name'); ?>" placeholder="Ví dụ: Đoàn Quý Dậu">
              <span class="error"><?php echo form_error('con_name'); ?></span>
            </td>
          </tr>
          <tr>
            <td>
              <strong>Email của bạn <em>*</em></strong>
            </td>
            <td class="input">
              <input type="text" name="con_email" value="<?php echo set_value('con_email'); ?>" placeholder="Ví dụ: example@example.com">
              <span class="error"><?php echo form_error('con_email'); ?></span>
            </td>
          </tr>
          <tr>
            <td>
              <strong>Địa chỉ nhận hàng <em>*</em></strong>
            </td>
            <td class="input">
              <input type="text" name="con_local1" value="<?php echo set_value('con_local1'); ?>" placeholder="Ví dụ: 175, Xuân Thuỷ, Cầu Giấy, Hà Nội">
              <span class="error"><?php echo form_error('con_local1'); ?></span>
            </td>
          </tr>
          <tr>
            <td>
              <strong>Địa chỉ nhận hàng số 2 <br />( Nếu địa chỉ số 1 không nhận được hàng)</strong>
            </td>
            <td class="input">
              <input type="text" name="con_local2" value="<?php echo set_value('con_local2'); ?>" placeholder="Ví dụ: 175, Xuân Thuỷ, Cầu Giấy, Hà Nội">
            </td>
          </tr>
          <tr>
            <td>
              <strong>Số điện thoại <em>*</em></strong>
            </td>
            <td class="input">
              <input type="text" name="con_phone1" value="<?php echo set_value('con_phone1'); ?>" placeholder="Ví dụ: 01234567890">
              <span class="error"><?php echo form_error('con_phone1'); ?></span>
            </td>
          </tr>
          <tr>
            <td>
              <strong>Số điện thoại liên lạc khác</strong>
            </td>
            <td class="input">
              <input type="text" name="con_phone2" value="<?php echo set_value('con_phone2'); ?>" placeholder="Ví dụ: 01234567890">
            </td>
          </tr>
          <tr>
            <td></td>
            <td><button class="red_btn" type="red_btn" disabled="disabled">Tiếp tục bước 3</button></td>
          </tr>
        </table>
      </div>
    </div>

    <div class="checkout_outer">
      <h2>Bước 3 : <span>Chọn phương thức giao hàng</span></h2>
      <div class="checkout_content billing_account clearfix">
        <table>
          <tr>
            <td><input type="radio" name="con_phuongthuc" value="0"></td>
            <td class="input">
               : <strong>Giao hàng tiêu chuẩn (3-4 ngày):</strong>
            </td>
          </tr>
          <tr>
            <td><input type="radio" name="con_phuongthuc" value="1"></td>
            <td class="input">
               : <strong>Chuyển phát nhanh (1-2 ngày):</strong>
            </td>
          </tr>
          <tr>
            <td><span class="error"><?php echo form_error('con_phuongthuc'); ?></span></td>
            <td><button class="red_btn" disabled="disabled">Tiếp tục bước 4</button></td>
          </tr>
        </table>
      </div>
    </div>

    <div class="checkout_outer">
      <h2>Bước 4 : <span>Phương thức thanh toán</span></h2>
      <div class="checkout_content billing_account clearfix">
        <table>
          <tr>
            <td><input type="radio" name="con_thanhtoan" value="0"></td>
            <td class="input">
               : <strong>Thanh toán khi giao hàng:</strong>
            </td>
          </tr>
          <tr>
            <td><input type="radio" name="con_thanhtoan" value="1"></td>
            <td class="input">
               : <strong>Chuyển qua tài khoản ngân hàng</strong>
            </td>
          </tr>
          <tr>
            <td><span class="error"><?php echo form_error('con_thanhtoan'); ?></span></td>
            <td><button class="red_btn" disabled="disabled">Tiếp tục bước 5</button></td>
          </tr>
        </table>
      </div>
    </div>

    <div class="checkout_outer">
      <h2>Bước 5 : <span>Xác nhận hoá đơn</span></h2>
      <div class="checkout_content billing_account clearfix">
        <table>
          <tr>
            <td class="input"><button style="color: #000" type="submit" class="red_btn" name="sendorder">Xác nhận hóa đơn</button></td>
            <td class="pull-right"><a href="<?php echo base_url().'gio-hang/xoa-tat-ca-san-pham/'; ?>" >Hủy bỏ đơn hàng</a></td>
          </tr>
        </table>
      </div><!--end checkout_content-->
    </div><!--end checkout_outer-->
</form>
  </div><!--end sixteen-->
</div>