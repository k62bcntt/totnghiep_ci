<script type="text/javascript" src="<?php echo base_url();?>public/home/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url();?>public/home/js/jquery-migrate-1.0.0.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>public/home/plugins/bootstrap/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/home/js/dropKick/dropkick.css">
<script type="text/javascript" src="<?php echo base_url();?>public/home/js/dropKick/jquery.dropkick-1.0.0.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>public/home/js/jquery.nicescroll.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>public/home/js/jquery.tweet.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>public/home/js/jquery.cycle2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>public/home/js/jquery.cycle2.tile.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>public/home/js/jcarousellite_1.0.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/home/js/fancybox/jquery.fancybox-1.3.4.css">
<script type="text/javascript" src="<?php echo base_url();?>public/home/js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>public/home/js/jquery.etalage.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>public/home/js/jquery.cookie.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>public/home/js/main.js"></script>


<!-- giỏ hàng -->
<script type="text/javascript">
    $(document).ready(function() {
    	var gtri;
        var id;
        $(".max_qty").click(function(){
            id = $(this).attr('stt');
        	gtri = parseInt($("#val_qty_"+id).val());
            var tang = gtri + 1;
            $("#val_qty_"+id).val(tang);
        });   
        $(".min_qty").click(function(){
            id = parseInt($(this).attr('stt'));
        	gtri = parseInt($("#val_qty_"+id).val());
            var giam = gtri - 1;
            $("#val_qty_"+id).val(giam);
        });
    });
</script>