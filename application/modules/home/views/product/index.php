<div class="container">
	<div class="sixteen columns">
		<div class="latest">
			
			<div class="box_head">
				<h3>Featured Items</h3>
				<div class="pagers center">
					<a class="prev latest_prev" href="#prev">Prev</a>
					<a class="nxt latest_nxt" href="#nxt">Next</a>
				</div>
			</div><!--end box_head -->

			<div class="cycle-slideshow" 
	        data-cycle-fx="scrollHorz"
	        data-cycle-timeout=0
	        data-cycle-slides="> ul"
	        data-cycle-prev="div.pagers a.latest_prev"
	        data-cycle-next="div.pagers a.latest_nxt"
	        >
				<ul class="product_show">
					<li>
						<div class="img">
							<div class="hover_over">
								<a class="link" href="#">link</a>
								<a class="cart" href="#">cart</a>
							</div>
							<a href="#">
								<img src="images/photos/anh_sanpham.jpg" alt="product">
							</a>
						</div>
						<h6><a href="#">Tên sản phẩm</a></h6>
						<h5>$40.90</h5>
					</li>
					<li>
						<div class="img">
							<div class="hover_over">
								<a class="link" href="#">link</a>
								<a class="cart" href="#">cart</a>
							</div>
							<a href="#">
								<img src="images/photos/anh_sanpham.jpg" alt="product">
							</a>
						</div>
						<h6><a href="#">Tên sản phẩm</a></h6>
						<h5>$130.90</h5>
					</li>
					<li>
						<div class="img">
							<div class="hover_over">
								<a class="link" href="#">link</a>
								<a class="cart" href="#">cart</a>
							</div>
							<a href="#">
								<img src="images/photos/anh_sanpham.jpg" alt="product">
							</a>
						</div>
						<h6><a href="#">Tên sản phẩm</a></h6>
						<h5>$200.00</h5>
					</li>
					<li>
						<div class="img">
							<div class="hover_over">
								<a class="link" href="#">link</a>
								<a class="cart" href="#">cart</a>
							</div>
							<a href="#">
								<img src="images/photos/anh_sanpham.jpg" alt="product">
							</a>
						</div>
						<h6><a href="#">Tên sản phẩm</a></h6>
						<h5>$358.00</h5>
					</li>
				</ul>
				<ul class="product_show">
					<li>
						<div class="img">
							<div class="hover_over">
								<a class="link" href="#">link</a>
								<a class="cart" href="#">cart</a>
							</div>
							<a href="#">
								<img src="images/photos/anh_sanpham.jpg" alt="product">
							</a>
						</div>
						<h6><a href="#">Tên sản phẩm</a></h6>
						<h5>$40.90</h5>
					</li>
					<li>
						<div class="img">
							<div class="hover_over">
								<a class="link" href="#">link</a>
								<a class="cart" href="#">cart</a>
							</div>
							<a href="#">
								<img src="images/photos/anh_sanpham.jpg" alt="product">
							</a>
						</div>
						<h6><a href="#">Tên sản phẩm</a></h6>
						<h5>$130.90</h5>
					</li>
					<li>
						<div class="img">
							<div class="hover_over">
								<a class="link" href="#">link</a>
								<a class="cart" href="#">cart</a>
							</div>
							<a href="#">
								<img src="images/photos/anh_sanpham.jpg" alt="product">
							</a>
						</div>
						<h6><a href="#">Tên sản phẩm</a></h6>
						<h5>$200.00</h5>
					</li>
					<li>
						<div class="img">
							<div class="hover_over">
								<a class="link" href="#">link</a>
								<a class="cart" href="#">cart</a>
							</div>
							<a href="#">
								<img src="images/photos/anh_sanpham.jpg" alt="product">
							</a>
						</div>
						<h6><a href="#">Tên sản phẩm</a></h6>
						<h5>$358.00</h5>
					</li>
				</ul>
				
			</div>
		</div><!--end latest-->
	</div><!--end eight-->
	
	<div class="sixteen columns">
		<div class="featured">

			<div class="box_head">
			<h3>Đặc điểm sản phẩm</h3>
				<div class="pagers center">
					<a class="prev featuredPrev" href="#prev">Prev</a>
					<a class="nxt featuredNxt" href="#nxt">Next</a>
				</div>
			</div><!--end box_head -->

			<div class="cycle-slideshow" 
	        data-cycle-fx="scrollHorz"
	        data-cycle-timeout=0
	        data-cycle-slides="> ul"
	        data-cycle-prev="div.pagers a.featuredPrev"
	        data-cycle-next="div.pagers a.featuredNxt"
	        >
				<ul class="product_show">
					<li>
						<div class="img">
							<div class="hover_over">
								<a class="link" href="#">link</a>
								<a class="cart" href="#">cart</a>
							</div>
							<a href="#">
								<img src="images/photos/anh_sanpham.jpg" alt="product">
							</a>
						</div>
						<h6><a href="#">Tên sản phẩm</a></h6>
						<h5>$130.90</h5>
					</li>
					<li>
						<div class="img">
							<div class="offer_icon"></div>
							<div class="hover_over">
								<a class="link" href="#">link</a>
								<a class="cart" href="#">cart</a>
							</div>
							<a href="#">
								<img src="images/photos/anh_sanpham.jpg" alt="product">
							</a>
						</div>
						<h6><a href="#">Tên sản phẩm</a></h6>
						<h5><span class="sale_offer">$330.00</span>&nbsp;&nbsp;&nbsp;&nbsp;$130.00</h5>
					</li>
					<li>
						<div class="img">
							<div class="offer_icon"></div>
							<div class="hover_over">
								<a class="link" href="#">link</a>
								<a class="cart" href="#">cart</a>
							</div>
							<a href="#">
								<img src="images/photos/anh_sanpham.jpg" alt="product">
							</a>
						</div>
						<h6><a href="#">Tên sản phẩm</a></h6>
						<h5><span class="sale_offer">$210.00</span>&nbsp;&nbsp;&nbsp;&nbsp;$194.90</h5>
					</li>
					<li>
						<div class="img">
							<div class="hover_over">
								<a class="link" href="#">link</a>
								<a class="cart" href="#">cart</a>
							</div>
							<a href="#">
								<img src="images/photos/anh_sanpham.jpg" alt="product">
							</a>
						</div>
						<h6><a href="#">Tên sản phẩm</a></h6>
						<h5>$130.90</h5>
					</li>
				</ul>
				<ul class="product_show">
					<li>
						<div class="img">
							<div class="hover_over">
								<a class="link" href="#">link</a>
								<a class="cart" href="#">cart</a>
							</div>
							<a href="#">
								<img src="images/photos/anh_sanpham.jpg" alt="product">
							</a>
						</div>
						<h6><a href="#">Tên sản phẩm</a></h6>
						<h5>$130.90</h5>
					</li>
					<li>
						<div class="img">
							<div class="offer_icon"></div>
							<div class="hover_over">
								<a class="link" href="#">link</a>
								<a class="cart" href="#">cart</a>
							</div>
							<a href="#">
								<img src="images/photos/anh_sanpham.jpg" alt="product">
							</a>
						</div>
						<h6><a href="#">Tên sản phẩm</a></h6>
						<h5><span class="sale_offer">$330.00</span>&nbsp;&nbsp;&nbsp;&nbsp;$130.00</h5>
					</li>
					<li>
						<div class="img">
							<div class="offer_icon"></div>
							<div class="hover_over">
								<a class="link" href="#">link</a>
								<a class="cart" href="#">cart</a>
							</div>
							<a href="#">
								<img src="images/photos/anh_sanpham.jpg" alt="product">
							</a>
						</div>
						<h6><a href="#">Tên sản phẩm</a></h6>
						<h5><span class="sale_offer">$210.00</span>&nbsp;&nbsp;&nbsp;&nbsp;$194.90</h5>
					</li>
					<li>
						<div class="img">
							<div class="hover_over">
								<a class="link" href="#">link</a>
								<a class="cart" href="#">cart</a>
							</div>
							<a href="#">
								<img src="images/photos/anh_sanpham.jpg" alt="product">
							</a>
						</div>
						<h6><a href="#">Tên sản phẩm</a></h6>
						<h5>$130.90</h5>
					</li>
				</ul>
			</div>
		</div><!--end featured-->
	</div><!--end eight-->
</div>