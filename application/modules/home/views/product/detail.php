<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/home/css/blog.css">
<?php  if(isset($infos) && !empty($infos)): ?>
<div class="container">
	<div class="sixteen columns">
		
		<div id="pageName">
			<div class="name_tag">
				<p>
					Bạn đang ở :: <a href="<?php echo base_url(); ?>">Trang chủ</a> :: <a href="<?php echo base_url().'danh-muc'; ?>">Sản phẩm</a> :: <?= $infos['pro_name'] ?>
				</p>
				<div class="shapLeft"></div>
				<div class="shapRight"></div>
			</div>
		</div><!--end pageName-->

	</div>
</div>
<div class="container">
	<div class="eleven columns">
		<article>
			<div class="blogImg">
				<div class="pagers center">
					<a class="prev blog_slide_prev" href="#prev">Prev</a>
					<a class="nxt blog_slide_nxt" href="#nxt">Next</a>
				</div>

	        	<ul class="cycle-slideshow"
				data-cycle-fx="tileBlind"
		        data-cycle-timeout=0
		        data-cycle-slides="> li"
		        data-cycle-prev="div.pagers a.blog_slide_prev"
		        data-cycle-next="div.pagers a.blog_slide_nxt"
		        >
	        		<li><a href="#"><img src="<?= $infos['pro_images'] ?>" alt="$infos['pro_name']"></a></li>
	        		
	        	</ul>
			</div>

			<div class="blogDesc clearfix">
				<div class="blogLeft clearfix">
					<h6><?php echo $infos['updated_at']; ?></h6>
					<ul>
						<li>Favorites: <a href="#">13</a></li>
						<li>Author: <a href="#">John Doe</a></li>
						<li>Views: <a href="#">113 Times</a></li>
						<li><div class="fb-like" data-href="<?php echo base_url().'chi-tiet-san-pham/'.$infos['pro_name_rewrite'].'-'.$infos['pro_id']; ?>" data-layout="button" data-action="like" data-show-faces="true" data-share="true"></div></li>
						<li>
							<div class="fb-save" data-uri="<?php echo base_url().'chi-tiet-san-pham/'.$infos['pro_name_rewrite'].'-'.$infos['pro_id'].'/'; ?>" data-size="small"></div>
						</li>		
					</ul>
				</div>

				<div class="blogRight">
					<h5>
						<a href="#"><?= $infos['pro_name'] ?></a>
					</h5>
					<h2>
						<?= 'Giá bán: <span style="color:#E81962;">'.$infos['pro_price'].'</span> VND' ?>	
					</h2>
					<br />
					<a class="gray_btn" id="addCart" href="<?php echo base_url().'them-gio-hang/'.$infos['pro_id']; ?>">Thêm vào giỏ hàng</a>
					<hr>
					<p>
						<?= $infos['pro_full'] ?>
					</p>

				</div>
			</div>

		</article>

		<section id="userComments">
			<div class="box_head">
				<h3>Bình luận</h3>
			</div>

			<div class="fb-comments" data-href="<?php echo base_url().'chi-tiet-san-pham/'.$infos['pro_name_rewrite'].'-'.$infos['pro_id']; ?>" data-width="640px" data-numposts="5"></div>

		</section>
	</div>

	<aside class="five columns">
		<div class="blogTab">
			<div id="tabs">
				<ul class="tabNav">
					<li><a class="currentTab" href="#recent">Mới</a></li>
					<li><a href="#puplar">Hot</a></li>
					<li><a href="#comment">Bán chạy</a></li>
					<div class="clear"></div>
				</ul>
				<div id="tabContentOuter">
					<div id="recent" class="tabContent">
					<?php if(isset($pro_new) && !empty($pro_new)): ?>
						<?php foreach ($pro_new as $key => $value):?>
						<div>
							<img src="<?php echo $value['pro_images'] ?>" alt="">
							<div>
								<a href="<?php echo base_url().'chi-tiet-san-pham/'.$value['pro_name_rewrite'].'-'.$value['pro_id']; ?>">
								<p><?php echo $value['pro_name']; ?></p>
								</a>
								<span><?php echo (!empty($value['pro_price']) ? $value['pro_price'] : '0').' VND'; ?></span>
							</div>
						</div>
						<?php endforeach; ?>
					<?php endif; ?>
						<div class="clear"></div>
					</div><!--end recent-->


					<div id="puplar" class="tabContent">
						<?php if(isset($pro_hot) && !empty($pro_hot)): ?>
							<?php foreach ($pro_hot as $key => $value):?>
							<div>
								<img src="<?php echo $value['pro_images'] ?>" alt="">
								<div>
									<a href="<?php echo base_url().'chi-tiet-san-pham/'.$value['pro_name_rewrite'].'-'.$value['pro_id']; ?>">
									<p><?php echo $value['pro_name']; ?></p>
									</a>
									<span><?php echo (!empty($value['pro_price']) ? $value['pro_price'] : '0').' VND'; ?></span>
								</div>
							</div>
							<?php endforeach; ?>
						<?php endif; ?>
						<div class="clear"></div>
					</div><!--end puplar-->


					<div id="comment" class="tabContent">
						<?php if(isset($pro_bestsale) && !empty($pro_bestsale)): ?>
							<?php foreach ($pro_bestsale as $key => $value):?>
							<div>
								<img src="<?php echo $value['pro_images'] ?>" alt="">
								<div>
									<a href="<?php echo base_url().'chi-tiet-san-pham/'.$value['pro_name_rewrite'].'-'.$value['pro_id']; ?>">
									<p><?php echo $value['pro_name']; ?></p>
									</a>
									<span><?php echo (!empty($value['pro_price']) ? $value['pro_price'] : '0').' VND'; ?></span>
								</div>
							</div>
							<?php endforeach; ?>
						<?php endif; ?>
						<div class="clear"></div>
					</div><!--end comment-->
				</div><!--end tabContentOuter-->
			</div><!--end tabs-->
		</div>
		<div class="blog_category clearfix">
			<div class="box_head">
				<h3>Chúng tôi trên Facebook</h3>
			</div>
			<div class="fb-page" data-href="<?php echo $config['facebook']; ?>" data-width="280px" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="<?php echo $config['facebook']; ?>"><a href="<?php echo $config['facebook']; ?>">Thời trang</a></blockquote></div></div>
		</div>
		<div class="ads">
			<h6>Quảng cáo</h6>
		</div><!--end ads-->
	</aside>
</div>
<?php endif; ?>