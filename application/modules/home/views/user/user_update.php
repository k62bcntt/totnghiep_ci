<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/home/css/cart.css">
<div class="container">
  <div class="sixteen columns">
    <div id="pageName" style="margin-bottom: -30px">
      <div class="name_tag">
        <p>
          Bạn đang ở đây :: <a href="#">Trang chủ</a> :: Blog
        </p>
        <div class="shapLeft"></div>
        <div class="shapRight"></div>
      </div>
    </div>
  </div>
</div>

<div class="container">
  <div class="sixteen columns">
      <div class="cart_tabs clearfix">
          <?php  
          if(isset($error)){
              echo '<div class="alert alert-danger">'.$error.'</div>';
          }
          ?>
          <?php  
          if(isset($success)){
              echo '<div class="alert alert-success">'.$success.'</div>';
          }
          ?>
          <ul class="cart_tabs_nav">
              <li><a class="active_tab" href="#estimate">Thông tin chi tiết</a></li>
              <li><a href="#discount_code">Thay đổi mật khẩu</a></li>
              <li><a href="#gift_voucher">Lịch sử mua hàng</a></li>
          </ul>
          <div class="cart_tabs_content">
              <div id="estimate" class="tab">
                  <p>Thông tin tài khoản</p>
                  <form method="post" action="" id="info" enctype="multipart/form-data">
                      <label for="country" class="clearfix">
                        <strong>Cập nhật anh đại diện</strong>
                        <img src="<?php echo $info['cus_image']; ?>" alt="<?php echo (!empty($info['cus_name']) ? $info['cus_name'] : '' ); ?>" width="220px" height="200px">
                      </label>
                      <label for="country" class="clearfix">
                        <strong></strong>
                          <input type="file" name="anhdaidien" >
                      </label>
                      <label for="country" class="clearfix">
                          <strong>Tên đăng nhập:</strong>
                          <input type="text" name="name" value="<?php echo (!empty($info['cus_name']) ? $info['cus_name'] : '' ); ?>" placeholder="Tên đăng nhập" disabled="disabled">
                      </label>
                      <label for="country" class="clearfix">
                          <strong>Họ và Tên: <em>*</em></strong>
                          <input type="text" name="fullname" value="<?php echo (!empty($info['cus_fullname']) ? $info['cus_fullname'] : '' ); ?>" placeholder="Họ và Tên ">
                      </label>
                      <?php echo form_error('fullname'); ?>
                      <label for="country" class="clearfix">
                          <strong>Địa chỉ: <em>*</em></strong>
                          <input type="text" name="address" value="<?php echo (!empty($info['cus_address']) ? $info['cus_address'] : '' ); ?>" placeholder="Địa chỉ">
                      </label>
                      <?php echo form_error("address"); ?>
                      <label for="country" class="clearfix">
                          <strong>Số điện thoại: <em>*</em></strong>
                          <input type="text" name="phone" value="<?php echo (!empty($info['cus_phone']) ? $info['cus_phone'] : '' ); ?>" placeholder="Số điện thoại">
                      </label>
                      <?php echo form_error("phone"); ?>

                      <strong>Giới tính: <em>*</em></strong>
                      <input type="radio" name="gender" value="1" <?php echo (isset($info['cus_gender']) && $info['cus_gender'] == 1) ? "checked" : '' ?>>
                      <h3 style="display: inline;"> Nam </h3>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      <input type="radio" name="gender" value="0" <?php echo (isset($info['cus_gender']) && $info['cus_gender'] == 0) ? "checked" : '' ?>>
                      <h3 style="display: inline;"> Nữ </h3>
                      <div class="clearfix"></div>
                      <?php echo form_error("gender"); ?>

                      <label for="country" class="clearfix">
                          <strong>Email: <em>*</em></strong>
                          <input type="email" name="email" value="<?php echo (!empty($info['cus_email']) ? $info['cus_email'] : '' ); ?>" placeholder="Email">
                      </label>
                      <?php echo form_error("email"); ?>
                      <div class="submit">
                          <input class="gray_btn" form="info" type="submit" name="update_info" value="Cập nhật thông tin">
                      </div>
                  </form>
              </div>


              <div id="discount_code" class="tab">
                  <p>Thay đổi mật khẩu</p>
                  <form method="post" action="" id="formpass">
                      <label for="" class="clearfix">
                          <strong>Mật khẩu cũ:</strong>
                          <input type="password" name="oldpassword">
                      </label>
                      <label for="" class="clearfix">
                          <strong>Mật khẩu mới:</strong>
                          <input type="password" name="password" value="" placeholder="********************">
                      </label>
                      <?php echo form_error("password"); ?>
                      <label for="" class="clearfix">
                          <strong>Nhập lại mật khẩu mới:</strong>
                          <input type="password" name="repass" value="" placeholder="********************">
                      </label>
                      <?php echo form_error("repass"); ?>
                      <div class="submit">
                          <input class="gray_btn" form="formpass" type="submit" name="update_pass" value="Cập nhật mật khẩu">
                      </div>
                  </form>
              </div>

              <div id="gift_voucher" class="tab">
                  <p>Lịch sử mua hàng</p>
                  <table class="cart_table">
                    <thead>
                      <tr>
                        <th class="first_td"><h4>Sản phẩm</h4></th>
                        <th><h4>Giá</h4></th>
                        <th><h4>Số lượng</h4></th>
                        <th><h4>Địa chỉ và <br /> Số điện thoại đặt hàng</h4></th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $tongtien2 = 0; if(isset($my_order)  && is_array($my_order)): ?>
                        <?php foreach ($my_order as $key => $value): ?>
                          <tr>
                            <td class="first_td">
                              <div class="clearfix">
                                <img src="<?php echo base_url().$value['image']; ?>" alt="<?php echo $value['pro_name']; ?>" width="120px" height="100px">
                                <span>
                                  <strong><?php echo $value['pro_name']; ?></strong>
                                </span>
                              </div>
                            </td>
                            <td>
                              <?php  echo '0'; ?>
                            </td>
                            <td>
                              <?php  echo $value['pro_qty']; ?>
                            </td>
                            <td>
                              <span><?php echo $value['local_phone']; ?></span>
                            </td>
                          </tr>
                        <?php endforeach; ?>
                      <?php endif; ?>
                    </tbody>
                  </table>
              </div>
          </div><!--end cart_tabs_content-->
      </div>
  </div>
</div>