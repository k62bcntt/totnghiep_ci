<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/home/css/user_log.css">
<div class="container">
  <div class="sixteen columns">
    <div id="pageName">
      <div class="name_tag">
        <p>
          Bạn đang ở :: <a href="<?php echo base_url(); ?>">Trang chủ</a> :: Đăng nhập hay đăng ký tài khoản mới
        </p>
        <div class="shapLeft"></div>
        <div class="shapRight"></div>
      </div>
    </div>
  </div>
</div>
<div class="container">
  <div class="sixteen columns">
  <?php 
    if(isset($message_error) && !empty($message_error))
    {
      ?>
      <div class="alert alert-danger">
        <?php echo $message_error; ?>
      </div>
      <?php 
    }

    if(isset($message_success) && !empty($message_success))
    {
      ?>
      <div class="alert alert-success">
        <?php echo $message_success; ?>
      </div>
      <?php 
    }
  ?>
  </div>
</div>
<div class="container">
    <div id="user_log" class="clearfix">
      <div class="nine columns">
        <div class="register">
          <div class="box_head">
            <h3>Tạo tài khoản</h3>
          </div>

          <h6>Bạn đã có tài khoản, hãy đăng nhập bên phải</h6>

          <form action="#" method="post" id="register">
            <ol>
              <li class="row clearfix">
                <label class="input_tag" for="firstName">Họ tên đầy đủ của bạn <em>*</em></label>
                <div class="inputOuter">
                <input id="firstName" type="text" name="fullname" value="<?php echo set_value('fullname'); ?>" placeholder="Đoàn Quý Dậu">
                <?php echo form_error('fullname'); ?>
                </div>
              </li>
              <li class="row clearfix">
                <label class="input_tag" for="email">E-Mail của bạn <em>*</em></label>
                <div class="inputOuter">
                <input id="email" type="text" name="email" value="<?php echo set_value('email'); ?>" placeholder="example@example.com">
                <?php echo form_error('email'); ?>
                </div>
              </li>
              <li class="row clearfix">
                <label class="input_tag" for="tele">Số điện thoại <em>*</em></label>
                <div class="inputOuter">
                <input id="tele" type="text" name="phone" value="<?php echo set_value('phone'); ?>" placeholder="0126598745 or 555-987-456">
                <?php echo form_error('phone'); ?>
                </div>
              </li>
              <li class="row clearfix">
                <label class="input_tag" for="fax">Địa chỉ của bạn</label>
                <div class="inputOuter">
                <input id="fax" type="text" name="local" value="" placeholder="Địa chỉ của bạn">
                </div>
              </li>
            </ol>

            <ol>
              <li class="row clearfix">
                <label class="input_tag" for="fax">Tên đăng nhập <em>*</em></label>
                <div class="inputOuter">
                <input id="username" type="text" name="username" value="<?php echo set_value('usename'); ?>" placeholder="Tên đăng nhập">
                <?php echo form_error('username'); ?>
                </div>
              </li>
              <li class="row clearfix">
                <label class="input_tag" for="chPass">Nhập mật khẩu <em>*</em></label>
                <div class="inputOuter">
                <input id="chPass" type="password" name="password" value="" placeholder="********************">
                </div>
              </li>
              <li class="row clearfix">
                <label class="input_tag" for="coPass">Nhập lại mật khẩu <em>*</em></label>
                <div class="inputOuter">
                <input id="coPass" type="password" name="con_pass" value="" placeholder="********************">
                </div>
              </li>
            </ol>

            <ol>
              <li class="row clearfix">
                <label class="input_tag">Giới tính <em>*</em></label>
                <div class="inputOuter">
                <input id="radio1" type="radio" name="gender" value="1">
                <label for="radio1">Nam</label>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <input id="radio2" type="radio" name="gender" value="0">
                <label for="radio2">Nữ</label>
                </div>
              </li>
            </ol>

            <ol>
              <li class="row clearfix">
                <label class="input_tag" for="checkbox1">Điều khoản bảo mật <em>*</em></label>
                <div class="inputOuter">
                <input type="checkbox" name="agress" value="1" id="checkbox1">
                <label for="checkbox1">Tôi đã đọc</label><br><br>
                Đọc và hiểu thêm về các điều lệ bên phải
                </div>
              </li>
            </ol>

            <ol>
              <li class="row clearfix">
                <label class="input_tag" for="firstName">&nbsp;</label>
                <div class="inputOuter button">
                <button type="submit" class="red_btn" form="register" name="btn_register">Đăng ký</button>
                </div>
              </li>
            </ol>
          </form>

        </div>
      </div><!--end nine-->



      <div class="seven columns">
        <div class="login">
          <div class="box_head">
            <h3>Đăng nhập</h3>
          </div><!--end box_head -->
          <div class="form-group" id="fb">
              <a href="<?php echo base_url('fblogin');?>">
                  <img src="<?php base_url()?>public/home/images/icons/facebook_login.png" alt="Đăng nhập bằng Facebook" width="80%" height="50px">
              </a>
          </div>
          
          <h6>Bạn chưa có tài khoản, hãy đăng ký phía bên trái</h6>

          <form method="post" action="" id="login">
            <ol>
              <li class="row clearfix">
                <label class="input_tag" for="user"> Tên đăng nhập <em>*</em></label>
                <div class="inputOuter">
                <input id="user" type="text" name="login_username" value="<?php echo set_value('login_username'); ?>" placeholder="example@example.com">
                <?php echo form_error('login_username'); ?>
                </div>
              </li>
              <li class="row clearfix">
                <label class="input_tag" for="pass">Password <em>*</em></label>
                <div class="inputOuter">
                <input id="pass" type="password" name="login_password" value="<?php echo set_value('login_username'); ?>" placeholder="**********">
                <?php echo form_error('login_pass'); ?>
                <small><a href="#">Quên mật khẩu?</a></small>
                </div>
              </li>
              <li class="row clearfix">
                <label class="input_tag" for="firstName">&nbsp;</label>
                <div class="inputOuter button">
                <button type="submit" class="red_btn" name="btn_login" form="login">Đăng nhập</button>
                </div>
              </li>
            </ol>
          </form>
        </div><!--end login-->
        <br /> <br /> <br />
        <div>
          <h2>Điều khoản và điều lệ</h2>
          <div style="border: none; padding: 5px; background: #fff; height: 300px; overflow-y: scroll;"><?php echo (!empty($config['dieukhoandieule']) ? $config['dieukhoandieule'] : 'Chưa có ... ' ); ?></div> 
        </div>
      </div><!--end seven-->

    </div><!--end user_log-->

</div>