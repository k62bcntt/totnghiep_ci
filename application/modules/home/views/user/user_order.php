<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/home/css/checkout.css">
<div class="container">
  <div class="sixteen columns">
    <div id="pageName">
      <div class="name_tag">
        <p>
          Bạn đang ở :: <a href="<?php echo base_url(); ?>">Trang chủ</a> :: Đơn hàng
        </p>
        <div class="shapLeft"></div>
        <div class="shapRight"></div>
      </div>
    </div><!--end pageName-->

  </div>
</div><!-- container -->

<div class="container">
  <div class="sixteen columns">
    <div class="box_head">
      <h3>Checkout</h3>
    </div><!--end box_head -->

    <div class="checkout_outer">
      <h2>Bước 1 : <span>Kiểm tra tài khoản</span></h2>
      <div class="checkout_content checkout_option clearfix">
        <div class="not_register">
          <h6>Nếu bạn chưa đăng ký, hãy đăng ký miễn phí ngay</h6>
          <p>
            Tài khoản mà bạn dùng ở đây để thực hiện các giao dịch và xác nhận các thông tin mua bán
          </p>
          <a class="gray_btn" href="<?php echo base_url().'dang-ky-va-dang-nhap'; ?>">Đăng ký ngay</a>
        </div>
        <form mehod="#" action="#">
          <h6>Nếu đã có tài khoản, hãy đăng nhập</h6>
          <label>
            <strong>Tên đăng nhập hay email <em>*</em></strong>
            <input type="text" name="email" value="" placeholder="example@example.com">
          </label>
          <label>
            <strong>Mật khẩu <em>*</em></strong>
            <input type="text" name="password" value="" placeholder="**********">
          </label>
          <div class="submit">
            <input class="red_btn" type="submit" name="submit" value="Đăng nhập">
          </div>
        </form>
      </div><!--end checkout_content-->
    </div><!--end checkout_outer-->

    <div class="checkout_outer">
      <h2>Bước 2 : <span>Thông tin nhận hàng </span></h2>
      <div class="checkout_content billing_account clearfix">
        <form action="#" method="#">
        <table>
          <tr>
            <td>
              <strong>Họ và tên đầy đủ của bạn <em>*</em></strong>
            </td>
            <td class="input">
              <input type="text" name="#" value="" placeholder="Ví dụ: Quý Dậu">
            </td>
          </tr>
          <tr>
            <td>
              <strong>Tên họ <em>*</em></strong>
            </td>
            <td class="input">
              <input type="text" name="#" value="" placeholder="Ví dụ: Đoàn">
            </td>
          </tr>
          <tr>
            <td>
              <strong>Địa chỉ số 1 <em>*</em></strong>
            </td>
            <td class="input">
              <input type="text" name="#" value="" placeholder="Ví dụ: 175, Xuân Thuỷ, Cầu Giấy, Hà Nội">
            </td>
          </tr>
          <tr>
            <td>
              <strong>Địa chỉ số 2</strong>
            </td>
            <td class="input">
              <input type="text" name="#" value="" placeholder="Ví dụ: 175, Xuân Thuỷ, Cầu Giấy, Hà Nội">
            </td>
          </tr>
          <tr>
            <td>
              <strong>Thành phố <em>*</em></strong>
            </td>
            <td class="input">
              <input type="text" name="#" value="" placeholder="Ví dụ: Hà Nội">
            </td>
          </tr>
          <tr>
            <td>
              <strong>Mã bưu điện <em>*</em></strong>
            </td>
            <td class="input">
              <input type="text" name="#" value="" placeholder="Ví dụ: 12345">
            </td>
          </tr>
          <tr>
            <td></td>
            <td><input class="red_btn" type="submit" name="submit" value="Tiếp tục"></td>
          </tr>
        </table>
        </form>
      </div><!--end checkout_content-->
    </div><!--end checkout_outer-->

    <div class="checkout_outer">
      <h2>Bước 3 : <span>Chi tiết giao hàng</span></h2>
      <div class="checkout_content clearfix">
        <!--content here-->
      </div><!--end checkout_content-->
    </div><!--end checkout_outer-->

    <div class="checkout_outer">
      <h2>Bước 4 : <span>Phương thức giao hàng</span></h2>
      <div class="checkout_content clearfix">
        <!--content here-->
      </div><!--end checkout_content-->
    </div><!--end checkout_outer-->

    <div class="checkout_outer">
      <h2>Bước 5 : <span>Phương thức thanh toán</span></h2>
      <div class="checkout_content clearfix">
        <!--content here-->
      </div><!--end checkout_content-->
    </div><!--end checkout_outer-->

    <div class="checkout_outer">
      <h2>Bước 6 : <span>Hoá đơn</span></h2>
      <div class="checkout_content clearfix">
        <!--content here-->
      </div><!--end checkout_content-->
    </div><!--end checkout_outer-->

    <div class="checkout_outer">
      <h2>Bước 7 : <span>Xác nhận hoá đơn</span></h2>
      <div class="checkout_content clearfix">
        <!--content here-->
      </div><!--end checkout_content-->
    </div><!--end checkout_outer-->


  </div><!--end sixteen-->
</div>