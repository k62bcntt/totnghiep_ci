<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Basic Page Needs
  	================================================== -->
	<meta charset="utf-8">
	<title><?php echo ((!isset($title) && !empty($title)) ? $title : $config['config_title']); ?></title>
	<meta name="description" content="">
	<meta name="author" content="Gà Rừng">
	<meta name="keywords" content="<?= isset($keyword) && $keyword != null ? $keyword : $config['config_key']; ?>" />
	<meta name="description" content="<?= isset($description) && $description != null ? $description : $config['config_des']; ?>" />
	<meta property="og:title" content="<?php echo isset($title) && $title != null ? $title : $config['config_title'] ; ?>" />
	<meta property="og:description" content="<?= isset($description) && $description != null ? $keyword : $config['config_des']; ?>" />
	<meta property="og:url" content="<?php echo base_url(); ?>" />
	<meta property="og:site_name" content="<?php echo $config['config_title']; ?>" />

	<!-- Thẻ hỗ trợ responsive mobile
  	================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- CSS
  	================================================== -->
	<?php $this->load->view("styles"); ?>
	<!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="<?php echo base_url(); ?>public/home/images/favicon.ico">
	<link rel="apple-touch-icon" href="<?php echo base_url(); ?>public/home/images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url(); ?>public/home/images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url(); ?>public/home/images/apple-touch-icon-114x114.png">
</head>
<body>
	<!-- FB
	=================================================== -->
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.6&appId=1696048927319976";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>
	<!-- =================================================== -->
	<?php $this->load->view("header"); ?>

	<?php 
		if(isset($slide)){
			$this->load->view('slide');
		} 
	?>

	<?php $this->load->view($template); ?>

	<?php $this->load->view("footer"); ?>
	<div id="sideWidget">
		<div class="bgPatterns">
			<h4>Solid Colors</h4>
			<a href="#" style="background:#fff">white</a>
			<a href="#" style="background:#fafafa">light_gray</a>
			<a href="#" style="background:#f7f7f7">gray</a>
			<br><br>

			<h4>Body Patterns</h4>
			<a href="#" style="background:url(<?php echo base_url(); ?>public/home/images/bg/white_carbon.png) repeat">white_carbon</a>

			<a href="#" style="background:url(<?php echo base_url(); ?>public/home/images/bg/circles.png) repeat">circles</a>

			<a href="#" style="background:url(<?php echo base_url(); ?>public/home/images/bg/cubes.png) repeat">cubes</a>

			<a href="#" style="background:url(<?php echo base_url(); ?>public/home/images/bg/exclusive_paper.png) repeat">exclusive_paper</a>

			<a href="#" style="background:url(<?php echo base_url(); ?>public/home/images/bg/gplaypattern.png) repeat">gplaypattern</a>

			<a href="#" style="background:url(<?php echo base_url(); ?>public/home/images/bg/large_leather.png) repeat">large_leather</a>

			<a href="#" style="background:url(<?php echo base_url(); ?>public/home/images/bg/lghtmesh.png) repeat">lghtmesh</a>

			<a href="#" style="background:url(<?php echo base_url(); ?>public/home/images/bg/light_wool.png) repeat">light_wool</a>

			<a href="#" style="background:url(<?php echo base_url(); ?>public/home/images/bg/lil_fiber.png) repeat">lil_fiber</a>

			<a href="#" style="background:url(<?php echo base_url(); ?>public/home/images/bg/snow.png) repeat">snow</a>

			<a href="#" style="background:url(<?php echo base_url(); ?>public/home/images/bg/soft_wallpaper.png) repeat">soft_wallpaper</a>

			<a href="#" style="background:url(<?php echo base_url(); ?>public/home/images/bg/weave.png) repeat">weave</a>

			<a href="#" style="background:url(<?php echo base_url(); ?>public/home/images/bg/white_brick_wall.png) repeat">white_brick_wall</a>

			<a href="#" style="background:url(<?php echo base_url(); ?>public/home/images/bg/white_paperboard.png) repeat">white_paperboard</a>

			<a href="#" style="background:url(<?php echo base_url(); ?>public/home/images/bg/white_tiles.png) repeat">white_tiles</a>

			<a href="#" style="background:url(<?php echo base_url(); ?>public/home/images/bg/wall4.png) repeat">wall4</a>

			<a href="#" style="background:url(<?php echo base_url(); ?>public/home/images/bg/furley_bg.png) repeat">furley_bg</a>

			<a href="#" style="background:url(<?php echo base_url(); ?>public/home/images/bg/extra_clean_paper.png) repeat">extra_clean_paper</a>

		</div>
		<a class="WidgetLink" href="#open">+</a>
	</div>
	<!-- JS
	================================================== -->
	<?php $this->load->view("scripts"); ?>

<!-- End Document
================================================== -->
</body>
</html>