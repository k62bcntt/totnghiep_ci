<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/home/css/home.css">
<div class="container">
<?php if(isset($productOfCates)  && !empty($productOfCates)): ?>	
	<?php foreach ($productOfCates as $key => $value): ?>
		<?php if(count($value['product']) == 0){ continue; } ?>
		<div class="sixteen columns">
			<div class="latest">
				<div class="box_head">
					<h3><?php echo $value['cate_name']; ?></h3>
					<div class="pagers center">
						<a class="prev latest_prev_<?php echo $key; ?>" href="#prev">Prev</a>
						<a class="nxt latest_nxt_<?php echo $key; ?>" href="#nxt">Next</a>

					</div>
				</div><!--end box_head -->

				<div class="cycle-slideshow" 
		        data-cycle-fx="scrollHorz"
		        data-cycle-timeout=0
		        data-cycle-slides="> ul"
		        data-cycle-prev="div.pagers a.latest_prev_<?php echo $key; ?>"
		        data-cycle-next="div.pagers a.latest_nxt_<?php echo $key; ?>"
		        >
		        	<?php 
					$dem = count($value['product']);
					$start = 0;
					$end = 4;
					while($end <= $dem){
						echo '<ul class="product_show">';
						for($i = $start; $i < $end; $i++)
						{
							?>
							<li>
								<div class="img">
									<div class="hover_over">
										<a class="link" href="<?php echo base_url().'chi-tiet-san-pham/'.$value['product'][$i]['pro_name_rewrite'].'-'.$value['product'][$i]['pro_id']; ?>">link</a>
										<a class="cart" href="<?php echo base_url().'them-gio-hang/'.$value['product'][$i]['pro_id']; ?>">cart</a>
									</div>
									<a href="#">
										<img src="<?php echo $value['product'][$i]['pro_images']; ?>" alt="<?php echo (isset($value['product'][$i]['pro_name']) ? $value['product'][$i]['pro_name'] : 'product'); ?>" width="100%" height="100%">
									</a>
								</div>
								<h6><a href="<?php echo base_url().'chi-tiet-san-pham/'.$value['product'][$i]['pro_name_rewrite'].'-'.$value['product'][$i]['pro_id']; ?>"><?php echo (isset($value['product'][$i]['pro_name']) ? $value['product'][$i]['pro_name'] : ''); ?></a></h6>
								<h5><?php echo (isset($value['product'][$i]['pro_price']) ? $value['product'][$i]['pro_price'] : ''); ?> VND</h5>
							</li>
							<?php
						}
						echo '</ul>';

						$start = $start + 4;
						$end = $end + 4;
						if($end > $dem)
						{
							break;
						}
					}
					if(($dem - $end) != 0)
					{
						echo '<ul class="product_show">';
						for($i = $start; $i < $dem; $i++)
						{
							?>
							<li>
								<div class="img">
									<div class="hover_over">
										<a class="link" href="<?php echo base_url().'chi-tiet-san-pham/'.$value['product'][$i]['pro_name_rewrite'].'-'.$value['product'][$i]['pro_id']; ?>">link</a>
										<a class="cart" href="<?php echo base_url().'them-gio-hang/'.$value['product'][$i]['pro_id']; ?>">cart</a>
									</div>
									<a href="#">
										<img src="<?php echo $value['product'][$i]['pro_images']; ?>" alt="<?php echo (isset($value['product'][$i]['pro_name']) ? $value['product'][$i]['pro_name'] : 'product'); ?>" width="100%" height="100%">
									</a>
								</div>
								<h6><a href="<?php echo base_url().'chi-tiet-san-pham/'.$value['product'][$i]['pro_name_rewrite'].'-'.$value['product'][$i]['pro_id']; ?>"><?php echo (isset($value['product'][$i]['pro_name']) ? $value['product'][$i]['pro_name'] : ''); ?></a></h6>
								<h5><?php echo (isset($value['product'][$i]['pro_price']) ? $value['product'][$i]['pro_price'] : ''); ?> VND</h5>
							</li>
							<?php
						}
						echo '</ul>';
					}
					?>
				</div>
				<a href="<?php echo base_url().'danh-muc/'.$value['cate_rewrite'].'-'.$value['cate_id'] ?>" class="red_btn" >Xem nhiều hơn ...</a>
			</div>
		</div>
	<?php endforeach; ?>
<?php endif; ?>
</div>