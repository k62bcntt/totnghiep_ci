<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/home/css/home.css">

<div class="container">	
	<div class="sixteen columns">
		<div class="latest">
		<?php if(isset($product)  && !empty($product)): ?>			
			<div class="box_head">
				<h3><?= $cate_name; ?></h3>
				<div class="pagers center">
					<a class="prev latest_prev" href="#prev">Prev</a>
					<a class="nxt latest_nxt" href="#nxt">Next</a>
				</div>
			</div><!--end box_head -->

			<div class="cycle-slideshow" 
	        data-cycle-fx="scrollHorz"
	        data-cycle-timeout=0
	        data-cycle-slides="> ul"
	        data-cycle-prev="div.pagers a.latest_prev"
	        data-cycle-next="div.pagers a.latest_nxt"
	        >
	        
				<?php 
				$dem = count($product);
				$start = 0;
				$end = 8;
				while($end <= $dem){

					echo '<ul class="product_show">';
					for($i = $start; $i < $end; $i++)
					{
						?>
						<li style="margin-right: 13px;">
							<div class="img">
								<div class="hover_over">
									<a class="link" href="<?php echo base_url().'chi-tiet-san-pham/'.$product[$i]['pro_name_rewrite'].'-'.$product[$i]['pro_id']; ?>">link</a>
									<a class="cart" href="<?php echo base_url().'them-gio-hang/'.$product[$i]['pro_id']; ?>">cart</a>
								</div>
								<a href="#">
									<img src="<?php echo $product[$i]['pro_images']; ?>" alt="<?php echo (isset($product[$i]['pro_name']) ? $product[$i]['pro_name'] : 'product'); ?>" width="100%" height="100%">
								</a>
							</div>
							<h6><a href="#"><?php echo (isset($product[$i]['pro_name']) ? $product[$i]['pro_name'] : ''); ?></a></h6>
							<h5><?php echo (isset($product[$i]['pro_price']) ? $product[$i]['pro_price'] : ''); ?> VND</h5>
						</li>
						<?php
					}
					echo '</ul>';

					$start = $start + 8;
					$end = $end + 8;
					if($end > $dem)
					{
						break;
					}
				}
				if(($dem-$end) != 0)
				{
					echo '<ul class="product_show">';
					for($i = $start; $i < $dem; $i++)
					{
						?>
						<li>
							<div class="img">
								<div class="hover_over">
									<a class="link" href="<?php echo base_url().'chi-tiet-san-pham/'.$product[$i]['pro_name_rewrite'].'-'.$product[$i]['pro_id']; ?>">link</a>
									<a class="cart" href="<?php echo base_url().'them-gio-hang/'.$product[$i]['pro_id']; ?>">cart</a>
								</div>
								<a href="<?php echo base_url().'chi-tiet-san-pham/'.$product[$i]['pro_name_rewrite'].'-'.$product[$i]['pro_id']; ?>">
									<img src="<?php echo $product[$i]['pro_images']; ?>" width="100%" height="100%" alt="<?php echo (isset($product[$i]['pro_name']) ? $product[$i]['pro_name'] : 'product'); ?>">
								</a>
							</div>
							<h6><a href="<?php echo base_url().'chi-tiet-san-pham/'.$product[$i]['pro_name_rewrite'].'-'.$product[$i]['pro_id']; ?>"><?php echo (isset($product[$i]['pro_name']) ? $product[$i]['pro_name'] : ''); ?></a></h6>
							<h5><?php echo (isset($product[$i]['pro_price']) ? $product[$i]['pro_price'] : ''); ?> VND</h5>
						</li>
						<?php
					}
					echo '</ul>';
				}
				?>
			</div>
		<?php endif; ?>
		</div>
	</div>
</div>