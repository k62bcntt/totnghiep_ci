<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/home/css/contact.css">
<div class="container">
	<div class="sixteen columns">
		
		<div id="pageName">
			<div class="name_tag">
				<p>
					Bạn đang ở :: <a href="<?php echo base_url(); ?>">Trang chủ</a> :: Liên hệ
				</p>
				<div class="shapLeft"></div>
				<div class="shapRight"></div>
			</div>
		</div><!--end pageName-->
	</div>
</div>

<div class="container">
	<div class="sixteen columns">
		<?php if (isset($_SESSION['added'])) { ?>
		<div class="col-md-12 alert-added">
			<div class="alert alert-success">
				<?php echo 'Cám ơn bạn đã liên hệ với website!'; ?>
			</div>
		</div>
		<?php unset($_SESSION['added']); } ?>
		<div class="eleven columns alpha">
			<div id="googleMap">
				<?php  
				if(isset($config['youtube']) && !empty($config['youtube']))
				{
					echo $config['youtube'];
				}
				?>
			</div>
		</div>

		<div class="five columns omega">
			<div class="blocked_info">
				<div>
					<h4>Địa chỉ</h4>
					<p>
						<?php echo $config['mienbac']; ?>
					</p>
				</div>
			</div>
			<div id="contact_info">
				<ul>
					<li class="contact_fax">
						<h6>Hotline</h6>
						<p><?php echo $config['hotline']; ?></p>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="sixteen columns">

		<div class="eleven columns alpha">
			<div id="contact_form">
				<div class="box_head">
					<h3>Gửi tin nhắn cho chúng tôi</h3>
				</div>

				<form action="#" name="contact" method="post">

					<div class="five columns alpha">
						<input name="con_name" value="<?php echo set_value('con_name'); ?>" type="text" placeholder="<?php echo '* Nhập họ tên... ';  ?>">
						<?php echo form_error('con_name') ?>

						<input name="con_email" value="<?php echo set_value('con_email'); ?>" type="email" placeholder="<?php echo  '* Nhập email...';  ?>">
						<?php echo form_error('con_email') ?>

						<input class="input_tool_tip" name="con_phone" type="text" placeholder="<?php echo'Nhập số điện thoại...'; ?>">
					</div>

					<div class="six columns omega">
						<textarea name="con_full" placeholder="* Tin nhắn của bạn ..."><?php echo set_value('con_full'); ?></textarea>
						<?php echo form_error('con_full') ?>
					</div>

					<div class="submitForm">
						<p>Trường ( * ) là bắt buộc.</p>
						<input class="red_btn" type="submit" name="addcontact" value="Gửi liên hệ">
						<div class="clear"></div>
					</div>

				</form><!--end form-->

			</div><!--end contact_form-->
		</div><!--end eleven-->


		<!--end five-->

	</div><!--end sixteen-->

	<div class="sixteen columns">
		<section id="tagLine" class="clearfix">
			<div class="twelve columns">
				<h5>
					Thời trang nữ <span>Hãy khám phá tất cả sản phẩm của chúng tôi.</span><br>
					<small>Chúc bạn có một ngày mua sắm vui vẻ</small>

				</h5>
			</div>
			<div class="three columns">
				<a class="red_btn" href="<?php echo base_url();  ?>">Khám phá</a>
			</div>
			
		</section><!--end tagLine-->
	</div><!--end sixteen-->
</div>