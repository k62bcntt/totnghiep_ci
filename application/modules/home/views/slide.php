<?php if(isset($pro_new) && !empty($pro_new)): ?>
<div class="container">
	<div class="sixteen columns">
		
		<div id="slide_outer">
			<div class="mainslide">

				<div class="pagers center">
					<a class="prev slide_prev" href="#prev">Prev</a>
					<a class="nxt slide_nxt" href="#nxt">Next</a>
				</div>

				<ul class="cycle-slideshow clearfix" 
		        data-cycle-fx="scrollHorz"
		        data-cycle-timeout="6000"
		        data-cycle-slides="> li"
		        data-cycle-pause-on-hover="true"
		        data-cycle-prev="div.pagers a.slide_prev"
		        data-cycle-next="div.pagers a.slide_nxt"
		        >
		        	<?php foreach ($pro_new as $pro): ?>
						<li class="clearfix">
							<div class="slide_img">
								<img src="<?php echo $pro['pro_images'];  ?>" alt="<?php echo $pro['pro_name']; ?>" width="100%" height="315px">
							</div>
							<div class="flex-caption">
								<h5>Sản phẩm <br><span><?php echo $pro['pro_name']; ?></span></h5>
								<p>
									<?php echo (!empty($pro['pro_des']) ? $pro['pro_des'] : '') ?>
								</p>
								<a href="<?php echo base_url().'chi-tiet-san-pham/'.$pro['pro_name'].'-'.$pro['pro_id'] ?>"><span>Xem thêm</span><span class="shadow"><?php echo (!empty($pro['pro_price']) ? $pro['pro_price'] : ''). ' VND' ?></span></a>
							</div>
						</li>
					<?php endforeach; ?>
				</ul>
			</div>
			<div class="shadow_left"></div>
			<div class="shadow_right"></div>
		</div>
	</div>
</div>
<?php endif; ?>