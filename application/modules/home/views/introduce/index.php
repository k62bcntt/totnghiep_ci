<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/home/css/about.css">
<div class="container">
	<div class="sixteen columns">
		
		<div id="pageName">
			<div class="name_tag">
				<p>
					Bạn đang ở :: <a href="<?php echo base_url(); ?>">Trang chủ</a> :: Giới thiệu
				</p>
				<div class="shapLeft"></div>
				<div class="shapRight"></div>
			</div>
		</div><!--end pageName-->

	</div>
</div>

<div class="container">
	<div class="sixteen columns">
		<div class="box_head">
			<h3>GIỚI THIỆU VỀ CHÚNG TÔI</h3>
		</div><!--end box_head -->

		<div class="five columns alpha">
			<div class="welcome_img">
				<div class="fb-page" data-href="<?php echo $config['facebook']; ?>" data-width="280px" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="<?php echo $config['facebook']; ?>"><a href="<?php echo $config['facebook']; ?>">Thời trang</a></blockquote></div></div>
			</div>
		</div><!--end four-->

		<div class="nine columns omega">
			<div class="welcome_text">
				<h2>Tự giới thiệu</h2>
				<p>
					<?php  
						echo (isset($gioithieu) && !empty($gioithieu)) ? $gioithieu['gioithieu'] : 'Chưa có bài giới thiệu';
					?>
				</p>
			</div>
		</div><!--end fourteen-->
	</div><!--end sixteen-->

	<div class="sixteen columns">
		<div id="tagLine" class="clearfix">
			<div class="twelve columns">
				<h5>
					Thời Trang Nữ <span>Hãy khám phá tất cả sản phẩm của chúng tôi từ đầu.</span><br>
					<small>Chúc bạn có một ngày mua sắm vui vẻ</small>
				</h5>
			</div>
			<div class="three columns">
				<a class="red_btn" href="<?php echo base_url().'danh-muc'; ?>">Khám phá</a>
			</div>
			
		</div>
	</div>


	<div class="sixteen columns">
		<div class="ourTeam">
			
			<div class="box_head">
				<h3>Nhân vật của shop</h3>
				<div class="pagers center">
					<a class="prev team_prev" href="#prev">Prev</a>
					<a class="nxt team_nxt" href="#nxt">Next</a>
				</div>
			</div><!--end box_head -->

			<div class="cycle-slideshow" 
	        data-cycle-fx="scrollHorz"
	        data-cycle-timeout=0
	        data-cycle-slides="> ul"
	        data-cycle-prev="div.pagers a.team_prev"
	        data-cycle-next="div.pagers a.team_nxt"
	        >
	        <?php if(isset($nv) && !empty($nv)): ?>
				<?php
				$dem = count($nv);
				$start = 0;
				$end = 4; 
				while($end <= $dem): 
				?>
				<ul class="team_show">
					<?php for($i = $start; $i < $end; $i++): ?>
					<li>
						<div class="img">
							<img src="<?php echo $nv[$i]['nv_image']; ?>" alt="product">
						</div>
						<h6><?php echo ($nv[$i]['nv_gender'] == 1 ? 'Ông: ' : 'Bà: ') ?> <strong><?php echo $nv[$i]['nv_name']; ?></strong></h6>
						<h5>Chức vụ: <?php echo $nv[$i]['nv_chucvu']; ?></h5>	
					</li>
					<?php 
					endfor; 
					$start += 4; 
					$end += 4;
					?>
				</ul>
				<?php  
				if($end > $dem)
				{
					break;
				}
				?>
				<?php endwhile; ?>
				<!-- ===============================================  -->
				<?php  
				if(($dem-$end) != 0)
				{
					echo '<ul class="team_show">';
					for($i = $start; $i < $dem; $i++): ?>
					<li>
						<div class="img">
							<img src="<?php echo $nv[$i]['nv_image']; ?>" alt="product">
						</div>
						<h2><?php echo ($nv[$i]['nv_gender'] == 1 ? 'Ông: ' : 'Bà: ') ?> <strong><?php echo $nv[$i]['nv_name']; ?></strong></h2>
						<h6>Chức vụ: <?php echo $nv[$i]['nv_chucvu']; ?></h6>	
					</li>
					<?php endfor;
					echo '</ul>';
				}
				?>
				<!-- ===============================================  -->
			<?php endif; ?>
			</div>
		</div>
	</div>

	<div class="sixteen columns">
		<div class="brands">

			<div class="box_head">
				<h3>brands</h3>
				<div class="pagers center">
					<a class="prev brand_prev" href="#prev">Prev</a>
					<a class="nxt brand_nxt" href="#nxt">Next</a>
				</div>
			</div><!--end box_head -->

			<div class="brandOuter">
				<ul>
				<?php if(isset($doitacs) && !empty($doitacs)): ?>
					<?php foreach ($doitacs as $key => $value): ?>
					<li>
						<img src="<?php echo $value['doitac_image']; ?>" alt="<?php echo (isset($value['pro_name']) ? $value['pro_name'] : 'product'); ?>">
					</li>
					<?php endforeach; ?>
				<?php endif; ?>
				</ul>
			</div>
		</div><!--end brands-->
	</div><!--end sixteen-->
</div>