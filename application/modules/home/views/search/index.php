<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/home/css/home.css">
<div class="container">
	<div class="sixteen columns">
		
		<div id="pageName" style="margin: 15px 0 -63px;">
			<div class="name_tag">
				<p>
					Bạn đang ở :: <a href="<?php echo base_url(); ?>">Trang chủ</a> :: Tìm kiếm theo sản phẩm
				</p>
				<div class="shapLeft"></div>
				<div class="shapRight"></div>
			</div>
		</div><!--end pageName-->
	</div>
</div>
<div class="container">
	<div class="sixteen columns">
		<div class="latest">
			<?php 
				if (!empty($listProduct)) {
			?>
			<div class="alert alert-success">
				Tìm thấy <?php echo count($listProduct) ?> sản phẩm phù hợp với từ khóa <?php echo $keyword ?>
			</div>
			<?php } else { ?>
			<div class="alert alert-danger">
					Không có sản phẩm nào phù hợp với từ khóa <?php echo $keyword ?>
			</div>
			<?php } ?>
			<div class="box_head">
				<h3><?php echo 'Những sản phẩm được tìm thấy!'; ?></h3>
				<div class="pagers center">
					<a class="prev latest_prev" href="#prev">Prev</a>
					<a class="nxt latest_nxt" href="#nxt">Next</a>
				</div>
			</div><!--end box_head -->

			<div class="cycle-slideshow" 
	        data-cycle-fx="scrollHorz"
	        data-cycle-timeout=0
	        data-cycle-slides="> ul"
	        data-cycle-prev="div.pagers a.latest_prev"
	        data-cycle-next="div.pagers a.latest_nxt"
	        >
	        	<?php 
				$dem = count($listProduct);
				$start = 0;
				$end = 12;
				while($end <= $dem){
					echo '<ul class="product_show">';
					for($i = $start; $i < $end; $i++)
					{
						?>
						<li style="margin-right: 13px;">
							<div class="img">
								<div class="hover_over">
									<a class="link" href="<?php echo base_url().'chi-tiet-san-pham/'.$listProduct[$i]['pro_name_rewrite'].'-'.$listProduct[$i]['pro_id']; ?>">link</a>
									<a class="cart" href="#">cart</a>
								</div>
								<a href="#">
									<img src="<?php echo $listProduct[$i]['pro_images']; ?>" alt="product" width="100%" height="100%">
								</a>
							</div>
							<h6><a href="<?php echo base_url().'chi-tiet-san-pham/'.$listProduct[$i]['pro_name_rewrite'].'-'.$listProduct[$i]['pro_id']; ?>"><?php echo (isset($listProduct[$i]['pro_name']) ? $listProduct[$i]['pro_name'] : ''); ?></a></h6>
							<h5><?php echo (isset($listProduct[$i]['pro_price']) ? $listProduct[$i]['pro_price'] : ''); ?> VND</h5>
						</li>
						<?php
					}

					echo '</ul>';
					$start = $start + 12;
					$end = $end + 12;
					if($end > $dem)
					{
						break;
					}
				}
				if(($dem - $end) != 0)
				{
					echo '<ul class="product_show">';
					for($i = $start; $i < $dem; $i++)
					{
						?>
						<li style="margin-right: 13px;">
							<div class="img">
								<div class="hover_over">
									<a class="link" href="<?php echo base_url().'chi-tiet-san-pham/'.$listProduct[$i]['pro_name_rewrite'].'-'.$listProduct[$i]['pro_id']; ?>">link</a>
									<a class="cart" href="#">cart</a>
								</div>
								<a href="#">
									<img src="<?php echo $listProduct[$i]['pro_images']; ?>" alt="product" width="100%" height="100%">
								</a>
							</div>
							<h6><a href="<?php echo base_url().'chi-tiet-san-pham/'.$listProduct[$i]['pro_name_rewrite'].'-'.$listProduct[$i]['pro_id']; ?>"><?php echo (isset($listProduct[$i]['pro_name']) ? $listProduct[$i]['pro_name'] : ''); ?></a></h6>
							<h5><?php echo (isset($listProduct[$i]['pro_price']) ? $listProduct[$i]['pro_price'] : ''); ?> VND</h5>
						</li>
						<?php
					}

					echo '</ul>';
				}
				?>
			</div>
		</div>
	</div>
</div>