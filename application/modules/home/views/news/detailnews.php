<div class="heroColor">
    <div class="container">
        <div class="row dichvu_breadcrumb allnews">
            <div class="col-md-12 col-sm-12 col-xs-12 hidden-xs hidden-md">
                <h2>
                <?php echo (($lang_page == 1) ? 'TIN TỨC' : 'NEWS'); ?></h2>
                <?php if($lang_page == 1): ?>
                        <ol class="breadcrumb hidden-xs hidden-sm pull-right">
                            <li><a href="<?php echo base_url(); ?>">Trang chủ</a></li>
                            <li><a href="<?php echo base_url().'tin-tuc'; ?>">Tin tức</a></li>
                            <li class="active" id="lastChild"><?php echo (!empty($title) ? substr($title, 6).'...' : 'Không có tiêu đề !'); ?></li>
                        </ol>
                    <?php else:  ?>
                        <ol class="breadcrumb hidden-xs hidden-sm">
                            <li><a href="<?php echo base_url(); ?>">Home Page</a></li>
                            
                            <li class="active" id="lastChild">News</li>
                        </ol>
                <?php endif;  ?>
            </div>
        </div>
    </div>
</div>
<div class="container two-col">
    <div class="container three-col container-fluid color">
        <div class="col-md-offset-2 col-md-10 color-bar green"></div>
    </div>
    <div class="row">
        <div class="col-md-9 col-sm-9 col-xs-8">
            <main>
                <article>
                    <h2> <?php echo (!empty($title) ? $title : 'Không có tiêu đề !'); ?></h2>
                    <p><i class="fa fa-calendar"></i><?php echo (!empty($infoItem['news_date']) ? $infoItem['news_date'] : 'Không rõ thời gian !'); ?></p>
                    <hr />
                    <p><?php echo (!empty($infoItem['news_full']) ? $infoItem['news_full'] : 'Không có nội dung !'); ?></p>
                    <!-- -------------------comment fb ----------------------  -->
                    <div class="fb-comments" data-href="http://www.facebook.com/doc24.vn" data-width="950px" data-numposts="5"></div>
                    <!-- -----------------------------------------  -->
                </article>
            </main>
        </div>
        <br />
        <div class="col-md-3 col-sm-3 col-xs-8 sidebar hidden-sm hidden-xs">
            <aside>
                <div class="photo-copy-promo">
                    <a target="Active Browser" class="standalone-link additional-resources"
                       href="<?php echo base_url().'tin-tuc'; ?>" title="Tin tức mới cập nhật">Tin tức mới cập nhật
                        <br>
                    </a>
                </div>
                
                <?php if(isset($lastNews) && !empty($lastNews)): ?>
                    <?php foreach ($lastNews as $key => $valuenews): ?>
                        <div class="standalone-link">
                            <h5 class="no-top"></h5>
                            <p><a href="<?php echo base_url().'tin-tuc/'.$valuenews['news_rewrite'].'-'.$valuenews['news_id']; ?>"
                                  title=""><img alt="" width="308" height="200" src="<?php echo base_url().'uploads/news/'.$valuenews['news_images']; ?>?la=en&amp;hash=A6150A908336B72304F536B10313DAF188D8CB54"></a>
                            </p>
                            <h4>
                                <a href="<?php echo base_url().'tin-tuc/'.$valuenews['news_rewrite'].'-'.$valuenews['news_id']; ?>"
                                   title="<?php echo (!empty($value['news_title']) ? $value['news_title'] : 'Không có tiêu đề'); ?>">
                                   <?php echo (!empty($valuenews['news_title']) ? $valuenews['news_title'] : 'Không có tiêu đề'); ?>
                                </a>
                            </h4>
                            <p><?php echo (!empty($valuenews['news_des']) ? $valuenews['news_des'] : '"Không có mô tả"'); ?></p>
                        </div>
                    <?php if($key == 3) break; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
            </aside>
        </div>
    </div>
</div>