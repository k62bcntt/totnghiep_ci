<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/home/css/blog.css">
<div class="container">
	<div class="sixteen columns">
		<div id="pageName">
			<div class="name_tag">
				<p>
					Bạn đang ở :: <a href="<?php echo base_url(); ?>">Trang chủ</a> :: Tin tức
				</p>
				<div class="shapLeft"></div>
				<div class="shapRight"></div>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="eleven columns">
	<?php if(isset($infoItem) && !empty($infoItem)): ?>
		<article>
			<?php if(!empty($infoItem['news_images'])): ?>
				<div class="blogImg">
					<div class="pagers center">
						<a class="prev blog_slide_prev" href="#prev">Prev</a>
						<a class="nxt blog_slide_nxt" href="#nxt">Next</a>
					</div>
		        	<ul class="cycle-slideshow"
					data-cycle-fx="tileBlind"
			        data-cycle-timeout=0
			        data-cycle-slides="> li"
			        data-cycle-prev="div.pagers a.blog_slide_prev"
			        data-cycle-next="div.pagers a.blog_slide_nxt"
			        >
		        		<li><a href="#"><img src="<?php echo $infoItem['news_images']; ?>" alt="<?php echo $infoItem['news_title']; ?>" style="max-height: 450px"></a></li>
		        	</ul>
				</div>
			<?php endif; ?>
			<div class="blogDesc clearfix">
				<div class="">
					<h5>
						<?php echo $infoItem['news_title']; ?>
					</h5>
					<?php echo "Người viết: ".$infoItem['authornews'].' | '.$infoItem['news_date']; ?>
					<hr />
					<p>
						<?php  echo $infoItem['news_full']; ?>
					</p>
				</div>
			</div>
		</article>
		<section id="addComment">
			<div class="box_head">
				<h3>Bình luận bài viết</h3>
			</div>
			<div class="fb-comments" data-href="https://www.facebook.com/thoitrangchuyende" data-width="640px" data-numposts="5"></div>
		</section>
	<?php endif; ?>
	</div>
	<aside class="five columns">
		<div class="blogTab">
			<div id="tabs">
				<ul class="tabNav">
					<li><a class="currentTab" href="#recent">Mới</a></li>
					<li><a href="#puplar">Hot</a></li>
					<li><a href="#comment">Bán chạy</a></li>
					<div class="clear"></div>
				</ul>
				<div id="tabContentOuter">
					<div id="recent" class="tabContent">
					<?php if(isset($pro_new) && !empty($pro_new)): ?>
						<?php foreach ($pro_new as $key => $value):?>
						<div>
							<img src="<?php echo $value['pro_images'] ?>" alt="">
							<div>
								<a href="<?php echo base_url().'chi-tiet-san-pham/'.$value['pro_name_rewrite'].'-'.$value['pro_id']; ?>">
								<p><?php echo $value['pro_name']; ?></p>
								</a>
								<span><?php echo (!empty($value['pro_price']) ? $value['pro_price'] : '0').' VND'; ?></span>
							</div>
						</div>
						<?php endforeach; ?>
					<?php endif; ?>
						<div class="clear"></div>
					</div><!--end recent-->


					<div id="puplar" class="tabContent">
						<?php if(isset($pro_hot) && !empty($pro_hot)): ?>
							<?php foreach ($pro_hot as $key => $value):?>
							<div>
								<img src="<?php echo $value['pro_images'] ?>" alt="">
								<div>
									<a href="<?php echo base_url().'chi-tiet-san-pham/'.$value['pro_name_rewrite'].'-'.$value['pro_id']; ?>">
									<p><?php echo $value['pro_name']; ?></p>
									</a>
									<span><?php echo (!empty($value['pro_price']) ? $value['pro_price'] : '0').' VND'; ?></span>
								</div>
							</div>
							<?php endforeach; ?>
						<?php endif; ?>
						<div class="clear"></div>
					</div><!--end puplar-->


					<div id="comment" class="tabContent">
						<?php if(isset($pro_bestsale) && !empty($pro_bestsale)): ?>
							<?php foreach ($pro_bestsale as $key => $value):?>
							<div>
								<img src="<?php echo $value['pro_images'] ?>" alt="">
								<div>
									<a href="<?php echo base_url().'chi-tiet-san-pham/'.$value['pro_name_rewrite'].'-'.$value['pro_id']; ?>">
									<p><?php echo $value['pro_name']; ?></p>
									</a>
									<span><?php echo (!empty($value['pro_price']) ? $value['pro_price'] : '0').' VND'; ?></span>
								</div>
							</div>
							<?php endforeach; ?>
						<?php endif; ?>
						<div class="clear"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="blog_category clearfix">
			<div class="box_head">
				<h3>Chúng tôi trên Facebook</h3>
			</div>
			<div class="fb-page" data-href="https://www.facebook.com/thoitrangchuyende/" data-width="280px" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/thoitrangchuyende/"><a href="https://www.facebook.com/thoitrangchuyende/">Thời trang</a></blockquote></div></div>
		</div>
		<div class="ads">
			<h6>Quảng cáo</h6>
		</div><!--end ads-->
	</aside>
</div>