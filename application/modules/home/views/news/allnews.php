<div class="blogEditorsPickWrapper blogsfeaturedlist">
    <div class="container-fluid blogEditorsPickTitle">
        <div class="col-md-12">
            <div class="dropdown">
                <h2 style="margin:auto;margin-top:20px;margin-bottom:20px;">
                    <?php echo ($lang_page == 1) ? 'Tin tức' : 'News' ?>
                </h2>
                <?php if($lang_page == 1): ?>
                        <ol class="breadcrumb hidden-xs hidden-sm">
                            <li><a href="<?php echo base_url(); ?>">Trang chủ</a></li>
                            
                            <li class="active" id="lastChild">Tin tức</li>
                        </ol>
                    <?php else:  ?>
                        <ol class="breadcrumb hidden-xs hidden-sm">
                            <li><a href="<?php echo base_url(); ?>">Home Page</a></li>
                            
                            <li class="active" id="lastChild">News</li>
                        </ol>
                <?php endif;  ?>
            </div>
        </div>
    </div>
    <div class="container-fluid blogEditorsPick" id="featuredblogs">
        <div class="blogitems">
        <?php if(isset($listNews) && !empty($listNews)): ?>
            <?php foreach ($listNews as $key => $news): ?>
                <div class="grid-content">
                    <div class="col-md-4 col-sm-12 post">
                        <div class="postImage">
                            <a href="<?php echo base_url().'tin-tuc/'.$news['news_rewrite'].'-'.$news['news_id']; ?>">
                                <img src="<?php echo $news['news_images']; ?>?h=280&amp;la=en&amp;w=600&amp;hash=130AC5808E78680ECFAEEE62752FBC797E588E20"
                                     alt="" width="600" height="280" Style="max-height:200px;min-height:200px"/>
                            </a>
                        </div>
                        <div class="postDescription">
                            <p class="author">
                                <a href="<?php echo base_url().'tin-tuc/'.$news['news_rewrite'].'-'.$news['news_id']; ?>"><?php echo (!empty($news['news_title']) ? $news['news_title'] : 'Không có tên'); ?></a>
                            </p>
                            <br />
                            <br />
                            <p class="date">
                                <?php echo (!empty($news['news_date']) ? $news['news_date'] : 'Không rõ'); ?>
                            </p>
                            <br />
                            <br />
                            <a href="<?php echo base_url().'tin-tuc/'.$news['news_rewrite'].'-'.$news['news_id']; ?>">
                                <span>
                                    <?php echo (!empty($news['news_des']) ? $news['news_des'] : 'Không có mô tả'); ?>
                                </span>
                            </a>
                            <br />
                            <br />
                            <p>
                                <a href="<?php echo base_url().'tin-tuc/'.$news['news_rewrite'].'-'.$news['news_id']; ?>"
                                   class="btn btn-lg drk-purple btn-primary" role="button">
                                    Đọc thêm<span class="icon icon-cta-arrow" aria-hidden="true"></span>
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
        </div>
    </div>
    <div class="clearfix visible-xs-block"></div>
    <div class="text-center">
        <div class="col-md-16 col-xs-16 center-text">
            <div id="page-selection" class="pagination">
                <?php echo $this->pagination->create_links(); ?>
            </div>
        </div>
    </div>
</div>
