<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/home/css/blog.css">
<div class="container">
	<div class="sixteen columns">
		<div id="pageName">
			<div class="name_tag">
				<p>
					Bạn đang ở :: <a href="<?php echo base_url(); ?>">Trang chủ</a> :: Tin tức
				</p>
				<div class="shapLeft"></div>
				<div class="shapRight"></div>
			</div>
		</div>
	</div>
</div>
<div class="container">

	<div class="eleven columns">
	<?php if(isset($listNews) && !empty($listNews)): ?>
		<?php foreach ($listNews as $key => $news): ?>
			<article>
				<div class="blogImg">

		       		<div class="pagers center">
						<a class="prev blog_slide_prev" href="#prev">Prev</a>
						<a class="nxt blog_slide_nxt" href="#nxt">Next</a>
					</div>

		        	<ul class="cycle-slideshow"
					data-cycle-fx="tileBlind"
			        data-cycle-timeout=0
			        data-cycle-slides="> li"
			        data-cycle-prev="div.pagers a.blog_slide_prev"
			        data-cycle-next="div.pagers a.blog_slide_nxt"
			        >
		        		<li><a href="<?php echo base_url().'tin-tuc/'.$news['news_rewrite'].'-'.$news['news_id']; ?>"><img src="<?php echo $news["news_images"]; ?>" alt="<?php echo $news["news_title"] ?>" style="max-height: 250px"></a></li>
		        	</ul>
				</div><!--end blogImg-->

				<div class="blogDesc clearfix">
					<div class="blogLeft clearfix">
						<h6>
							<span>
								<?php
									$datetime = explode("-",$news['news_date']); 
									$date = explode("/",$datetime[1]);
									echo $date[0]; 
								?>
							</span><br>
							<?php echo $date[1].' - '.$date[2]; ?>
						</h6>
						<ul>
							<li>Người viết: <a href="#"><?php echo $news['authornews']; ?></a></li>
							<li>Lượt xem: <a href="#"><?php echo $news['news_view']; ?></a></li>
						</ul>
					</div><!--end blog left-->

					<div class="blogRight">
						<h5>
							<a href="<?php echo base_url().'tin-tuc/'.$news['news_rewrite'].'-'.$news['news_id']; ?>"><?php echo $news['news_title']; ?></a>
						</h5>
						<p>
							<?php echo $news['news_info']; ?>
							<br />
							<br />
							<a href="<?php echo base_url().'tin-tuc/'.$news['news_rewrite'].'-'.$news['news_id']; ?>"> Đọc thêm</a>
						</p>
					</div>
				</div>
			</article>
		<?php  endforeach; ?>
	<?php endif; ?>
	</div>
	<aside class="five columns">
		<div class="blogTab">
			<div id="tabs">
				<ul class="tabNav">
					<li><a class="currentTab" href="#recent">Mới</a></li>
					<li><a href="#puplar">Hot</a></li>
					<li><a href="#comment">Bán chạy</a></li>
					<div class="clear"></div>
				</ul>
				<div id="tabContentOuter">
					<div id="recent" class="tabContent">
					<?php if(isset($pro_new) && !empty($pro_new)): ?>
						<?php foreach ($pro_new as $key => $value):?>
						<div>
							<img src="<?php echo $value['pro_images'] ?>" alt="">
							<div>
								<a href="<?php echo base_url().'chi-tiet-san-pham/'.$value['pro_name_rewrite'].'-'.$value['pro_id']; ?>">
								<p><?php echo $value['pro_name']; ?></p>
								</a>
								<span><?php echo (!empty($value['pro_price']) ? $value['pro_price'] : '0').' VND'; ?></span>
							</div>
						</div>
						<?php endforeach; ?>
					<?php endif; ?>
						<div class="clear"></div>
					</div><!--end recent-->


					<div id="puplar" class="tabContent">
						<?php if(isset($pro_hot) && !empty($pro_hot)): ?>
							<?php foreach ($pro_hot as $key => $value):?>
							<div>
								<img src="<?php echo $value['pro_images'] ?>" alt="">
								<div>
									<a href="<?php echo base_url().'chi-tiet-san-pham/'.$value['pro_name_rewrite'].'-'.$value['pro_id']; ?>">
									<p><?php echo $value['pro_name']; ?></p>
									</a>
									<span><?php echo (!empty($value['pro_price']) ? $value['pro_price'] : '0').' VND'; ?></span>
								</div>
							</div>
							<?php endforeach; ?>
						<?php endif; ?>
						<div class="clear"></div>
					</div><!--end puplar-->


					<div id="comment" class="tabContent">
						<?php if(isset($pro_bestsale) && !empty($pro_bestsale)): ?>
							<?php foreach ($pro_bestsale as $key => $value):?>
							<div>
								<img src="<?php echo $value['pro_images'] ?>" alt="">
								<div>
									<a href="<?php echo base_url().'chi-tiet-san-pham/'.$value['pro_name_rewrite'].'-'.$value['pro_id']; ?>">
									<p><?php echo $value['pro_name']; ?></p>
									</a>
									<span><?php echo (!empty($value['pro_price']) ? $value['pro_price'] : '0').' VND'; ?></span>
								</div>
							</div>
							<?php endforeach; ?>
						<?php endif; ?>
						<div class="clear"></div>
					</div><!--end comment-->
				</div><!--end tabContentOuter-->
			</div><!--end tabs-->
		</div>
		<div class="blog_category clearfix">
			<div class="box_head">
				<h3>Chúng tôi trên Facebook</h3>
			</div>
			<div class="fb-page" data-href="https://www.facebook.com/thoitrangchuyende/" data-width="280px" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/thoitrangchuyende/"><a href="https://www.facebook.com/thoitrangchuyende/">Thời trang</a></blockquote></div></div>
		</div>
		<div class="ads">
			<h6>Quảng cáo</h6>
		</div><!--end ads-->
	</aside>
</div>