<?php
	class Morder extends CI_Model{
		protected $_table = "tbl_order";
		
		public function __construct()
		{
			parent::__construct();
			$this->load->database();
		}

		public function insertOrder($data)
		{
			 return $this->db->insert($this->_table,$data);
		}
		public function getOnceOrder($id)
		{
			$this->db->where('user_id',$id);
			return $this->db->get($this->_table)->row_array();
		}
		public function getOnceCart($order_id)
		{
			$this->db->where('order_id',$order_id);
			return $this->db->get('order_detail')->result_array();
		}
	}