<?php
class mnews extends CI_Model {
	protected $_table 	= 'tbl_news';
	protected $_id 		= 'news_id';
	protected $_lang 	= 'news_lang';
	protected $_status 	= 'news_status';

	public function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function getAll($limit = '', $start = '', $idCate = '') {
		if ($limit)
			$this->db->limit($limit, $start);
		if ($idCate)
			$this->db->where('cago_id', $idCate);
		$this->db->where($this->_status, '1');
		$this->db->order_by($this->_id, 'DESC');
		return $this->db->get($this->_table)->result_array();
	}

	public function getOnce($id) {
		$this->db->where($this->_id, $id);
		return $this->db->get($this->_table)->row_array();
	}

	public function count_all($idCate = ''){
		if ($idCate)
			$this->db->where('cago_id', $idCate);
		$this->db->where($this->_status, '1');
		return $this->db->count_all_results($this->_table);
	}

	public function getNameUser($id)
	{
		$this->db->select(array('user_fullname'));
		$this->db->where('user_id',$id);
		$result = $this->db->get("tbl_user")->row_array();
		return $result['user_fullname'];
	}
}