<?php 
class users_model extends CI_Model 
{
    public function __Construct() 
    { 
        parent::__Construct(); 
    } 

    public function getUsers($conditions = array(), $fields = '')
    {   
        if (count($conditions) > 0) 
            $this->db->where($conditions); 
            $this->db->from('tbl_user'); 
            $this->db->order_by("tbl_user.user_id", "asc"); 

        if ($fields != '') 
            $this->db->select($fields); 
        else 
            $this->db->select('tbl_user.user_id,tbl_user.user_name,tbl_user.user_email');
            $result = $this->db->get(); 

        return $result; 
    }//End of getUsers Function

    public function checkUser($name, $phone)
    {
        $this->db->where('user_name', $name);
        $this->db->where('user_phone', $phone);
        return $this->db->get('tbl_user')->num_rows();
    }

    public function checkUserName($name, $phone)
    {
        $this->db->where('user_name', $name);
        $this->db->where('user_phone !=', $phone);
        return $this->db->get('tbl_user')->num_rows();
    }

    public function insert($data){
        $this->db->insert('tbl_user', $data);
    }
}