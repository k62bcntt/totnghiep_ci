<?php
class mconfig extends CI_Model {
	protected $_table 	= 'tbl_config';
	protected $_lang 	= 'language';

	public function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function getData() {
		return $this->db->get($this->_table)->row_array();
	}
}