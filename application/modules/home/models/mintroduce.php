<?php
class mintroduce extends CI_Model {
	protected $_table 	= 'tbl_config';
	protected $_id 		= 'config_id';
	protected $_status 	= 'status';

	public function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function getIntroduce() {
		$this->db->select(array('gioithieu'));
		$this->db->where($this->_id, 1);
		return $this->db->get($this->_table)->row_array();
	}
}	