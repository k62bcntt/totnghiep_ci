<?php  
class mnhanvien extends CI_Model
{
	protected $table = "nhanvien";
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function getAll()
	{
		$this->db->where('nv_status','1');
		return $this->db->get($this->table)->result_array();
	}
}