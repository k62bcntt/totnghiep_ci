<?php
class mcustomer extends CI_Model {
	protected $_table 	= 'tbl_customer';
	protected $_type	= 'CUS';
	protected $_id 		= 'id';
	protected $_lang 	= 'language';
	protected $_status 	= 'status';

	public function __construct() {
		parent::__construct();
		$this->load->database();
	}
	
}	