<?php

class Category extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->helper("url");
		$this->load->library("string");
		$this->load->model("mcategory");
        $this->load->model("mproduct");
        $this->load->model("mconfig");
		$this->load->model("mindex");
        $this->load->model("msupport");
	}

	public function index()
	{
		//----------------------------------------

        $cate = $this->mindex->list_cate();
        if(!empty($cate))
        {
            foreach ($cate as $key => $value) {
                $list_pro_cate[$key]['product'] = $this->mindex->list_pro($value['cate_id'],12);
                // lấy giá trị ảnh
                if (!empty($list_pro_cate[$key]['product'])) {
                    foreach ($list_pro_cate[$key]['product'] as $k => $v) {
                        if ($v['pro_images']) {
                            @$images = unserialize($v['pro_images']);
                            $list_pro_cate[$key]['product'][$k]['pro_images'] = base_url('uploads/products/thumb/'.$images[0]);
                        }
                        else 
                            $list_pro_cate[$key]['product'][$k]['pro_images'] = base_url('public/admin/images/no-images.jpg');
                    }
                }
                $list_pro_cate[$key]['cate_name'] = ($value['cate_name']);
                $list_pro_cate[$key]['cate_id'] = ($value['cate_id']);
                $list_pro_cate[$key]['cate_rewrite'] = ($value['cate_rewrite']);
            }
        }
		$data['productOfCates'] = $list_pro_cate;

		//--------------- SYSTEM -----------------
        $data['title'] = 'Danh mục tổng hợp sản phẩm | Tất cả sản phẩm | '.base_url();
        $data['menus'] = $this->mcategory->getAll();
        $data['config'] = $this->mconfig->getData();
        $data['support'] = $this->msupport->list_sup(2,0);
        //----------------------------------------
		$data['template'] 	= 'category/index';
		$this->load->view('layout', $data);
	}

    public function detail()
    {
        //========================================
        $params             = $this->uri->segment(2);
        $id                 = (int)end(explode("-", $params));
        $oneCate = $this->mcategory->getOnce($id);
        $list_pro = $this->mproduct->list_pro($oneCate['cate_id']);
        $data['cate_name'] = $oneCate['cate_name'];

        if(!empty($list_pro))
        {
            foreach ($list_pro as $key => $value) {
                if (!empty($value['pro_images'])) {
                    @$images = unserialize($value['pro_images']);
                    $list_pro[$key]['pro_images'] = base_url('uploads/products/thumb/'.$images[0]);
                }
                else {
                    $list_pro[$key]['pro_images'] = base_url('public/admin/images/no-images.jpg');
                }
            }
        }
        $data['product'] = $list_pro;
        //--------------- SYSTEM -----------------
        $data['title'] = 'Danh mục cho loại sản phẩm | Sản phẩm theo loại | '.base_url();
        $data['menus'] = $this->mcategory->getAll();
        $data['config'] = $this->mconfig->getData();
        $data['support'] = $this->msupport->list_sup(2,0);
        //----------------------------------------
        $data['template']   = 'category/detail';
        $this->load->view('layout', $data);
    }

}
