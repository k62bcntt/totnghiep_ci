<?php
class News extends MY_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->helper("url");
		$this->load->library("string");
		$this->load->model("mproduct");
        $this->load->model("msupport");
	}
	public function index(){
		$data['infoCate']	= '';
		$config['base_url'] 	= base_url()."tin-tuc";
		$config['total_rows'] 	= $this->mnews->count_all();
		$config['per_page'] 	= "10";
		$config['uri_segment'] 	= "2";
		$config['next_link'] 	= "Next";
		$config['prev_link'] 	= "Prev";
		$config['first_link'] 	= "First";
		$config['last_link'] 	= "Last";
		$this->load->library("pagination");
		$this->pagination->initialize($config);
		$start = $this->uri->segment(2);
		if ($config['total_rows']) {
			$listNews = $this->mnews->getAll( $config['per_page'], $start);
			if (!empty($listNews)) {
				foreach($listNews as $k => $v) {
					if ($v['news_images']){ 
						$listNews[$k]['news_images'] = base_url('uploads/news/thumb/'.$v['news_images']);
					}
					else{ 
						$listNews[$k]['news_images'] = base_url('public/admin/images/no-images.jpg');
					}
					$listNews[$k]['authornews'] = $this->mnews->getNameUser($v['user_created']);
				}
			}
		} else
			$listNews = '';

		// System
		$data['config']   			= $this->mindex->getdata();
		$data['title'] 				= 'Tin tức về sản phẩm | Khuyến mại | '.base_url();
		$data['keyword']            = 'Tin tức';
        $data['description']        = 'Tin tức';

		$data['listNews']			= $listNews;
		//=============== product new ============
        $data['pro_new'] = $this->mproduct->list_pro_new(3,0);
        if(!empty($data['pro_new']))
        {
            foreach ($data['pro_new'] as $key => $pro) {
                if(!empty($pro['pro_images']))
                {
                    @$images = unserialize($pro['pro_images']);
                    $data['pro_new'][$key]['pro_images'] = base_url().'uploads/products/thumb/'.$images[0];
                }
                else
                {
                    $data['pro_new'][$key]['pro_images'] = base_url().'public/images/no-images.jpg';
                }
            }
        }
        //============== product hot ==============
        $data['pro_hot'] = $this->mproduct->list_pro_hot(3,0);
        if(!empty($data['pro_hot']))
        {
            foreach ($data['pro_hot'] as $key => $pro) {
                if(!empty($pro['pro_images']))
                {
                    @$images = unserialize($pro['pro_images']);
                    $data['pro_hot'][$key]['pro_images'] = base_url().'uploads/products/'.$pro['pro_folderimg'].'/'.$images[0];
                }
                else
                {
                    $data['pro_hot'][$key]['pro_images'] = base_url().'public/images/no-images.jpg';
                }
            }
        }
        //============== product sale ==============
        $data['pro_bestsale'] = $this->mproduct->list_pro_bestsale(3,0);
        if(!empty($data['pro_bestsale']))
        {
            foreach ($data['pro_bestsale'] as $key => $pro) {
                if(!empty($pro['pro_images']))
                {
                    @$images = unserialize($pro['pro_images']);
                    $data['pro_bestsale'][$key]['pro_images'] = base_url().'uploads/products/'.$pro['pro_folderimg'].'/'.$images[0];
                }
                else
                {
                    $data['pro_bestsale'][$key]['pro_images'] = base_url().'public/images/no-images.jpg';
                }
            }
        }
        //--------------- SYSTEM -----------------
        $data['menus'] = $this->mcategory->getAll();
        $data['config'] = $this->mconfig->getData();
        $data['support'] = $this->msupport->list_sup(2,0);

		$data['template'] 			= 'news/index';
		$this->load->view('layout', $data);
	}

	public function detail() {
		$params 			= $this->uri->segment(2);
		$id					= (int)end(explode("-", $params));
		$info 				= $this->mnews->getOnce($id);
		if (!empty($info)){ 
			if(!empty($info['news_images']))
			{
				$info['news_images'] = base_url('uploads/news/'.$info['news_images']);
			}
			$info['authornews'] = $this->mnews->getNameUser($info['user_created']);
		}

		$data['fullURL'] 	= uri_string();
		$data['infoItem']	= $info;

		// System
		$data['activeMenu']			= 'news';
		$data['configWeb']			= $this->_config;
		$data['config']   			= $this->mindex->getdata();
		$data['info'] 				= $get_setup = $this->mindex->get_setup();
		$data['title'] 				= $info['news_title'];
		$data['keyword']            = $info['news_key'];
        $data['description']        = $info['news_des'];
        //=============== product new ============
        $data['pro_new'] = $this->mproduct->list_pro_new(3,0);
        if(!empty($data['pro_new']))
        {
            foreach ($data['pro_new'] as $key => $pro) {
                if(!empty($pro['pro_images']))
                {
                    @$images = unserialize($pro['pro_images']);
                    $data['pro_new'][$key]['pro_images'] = base_url().'uploads/products/thumb/'.$images[0];
                }
                else
                {
                    $data['pro_new'][$key]['pro_images'] = base_url().'public/images/no-images.jpg';
                }
            }
        }
        //============== product hot ==============
        $data['pro_hot'] = $this->mproduct->list_pro_hot(3,0);
        if(!empty($data['pro_hot']))
        {
            foreach ($data['pro_hot'] as $key => $pro) {
                if(!empty($pro['pro_images']))
                {
                    @$images = unserialize($pro['pro_images']);
                    $data['pro_hot'][$key]['pro_images'] = base_url().'uploads/products/'.$pro['pro_folderimg'].'/'.$images[0];
                }
                else
                {
                    $data['pro_hot'][$key]['pro_images'] = base_url().'public/images/no-images.jpg';
                }
            }
        }
        //============== product sale ==============
        $data['pro_bestsale'] = $this->mproduct->list_pro_bestsale(3,0);
        if(!empty($data['pro_bestsale']))
        {
            foreach ($data['pro_bestsale'] as $key => $pro) {
                if(!empty($pro['pro_images']))
                {
                    @$images = unserialize($pro['pro_images']);
                    $data['pro_bestsale'][$key]['pro_images'] = base_url().'uploads/products/'.$pro['pro_folderimg'].'/'.$images[0];
                }
                else
                {
                    $data['pro_bestsale'][$key]['pro_images'] = base_url().'public/images/no-images.jpg';
                }
            }
        }
        //--------------- SYSTEM -----------------
        $data['menus'] = $this->mcategory->getAll();
        $data['config'] = $this->mconfig->getData();
        $data['support'] = $this->msupport->list_sup(2,0);

		$data['template'] 	= 'news/detail';
		$this->load->view('layout', $data);
	}
}