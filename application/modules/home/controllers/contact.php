<?php
	class contact extends MY_Controller{
		protected $_lang_page;

		public function __construct(){
			parent::__construct();
			$this->load->helper("url");
			$this->load->helper("form");
			$this->load->helper('captcha');
			$this->load->library("string");
			$this->load->library("session");
			$this->load->library('form_validation');
			$this->load->model('mcategory');
			$this->load->model('mconfig');
			$this->load->model("msupport");
			$this->load->model("mcustomer_contact");
			$this->_lang_page = $this->session->userdata('lang_page');
		}
		public function index(){
			if (isset($_POST['addcontact'])) {
				$this->_validation();
				$this->_messages();
				if($this->form_validation->run()){ 
					$data = $this->_getData();
					$this->mcustomer_contact->insertContact($data);
					$_SESSION['added'] = 1;
					redirect(base_url('lien-he'), 'location');
				}
			}


			//=================== System ========================
			$data['title'] = 'Liên hệ | '.base_url();
			$data['menus'] = $this->mcategory->getAll();
			$data['config'] = $this->mconfig->getData();
			$data['support'] = $this->msupport->list_sup(2,0);

			$data['template'] 		= 'contact/index';
			$this->load->view('layout', $data);
		}

		private function _validation() {
			if ($this->_lang_page == '1') {
				$this->form_validation->set_rules('con_name', 'Họ và tên', 'required|min_length[4]');
				$this->form_validation->set_rules('con_email', 'Email', 'required|email');
				$this->form_validation->set_rules('con_full', 'Nội dung', 'required');
			} else {
				$this->form_validation->set_rules('con_name', 'Fullname', 'required|min_length[4]');
				$this->form_validation->set_rules('con_email', 'Email', 'required|email');
				$this->form_validation->set_rules('con_full', 'Content', 'required');
			}
		}

		private function _messages() {
			if ($this->_lang_page == '1') {
				$this->form_validation->set_message('required', '%s không được để trống');
				$this->form_validation->set_message('min_length', '%s không được ít hơn %d ký tự');
				$this->form_validation->set_message('email', '%s không đúng định dạng');
			} else {
				$this->form_validation->set_message('required', '%s required');
				$this->form_validation->set_message('min_length', 'Input is too short, minimum is %d characters');
				$this->form_validation->set_message('email', 'Invalid %s format');
			}
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-validate">','</div>');
		}

		private function _getData() {
			$data = array(
				'con_name'		=> $this->input->post('con_name'),
				'con_email'		=> $this->input->post('con_email'),
				'con_phone'		=> $this->input->post('con_phone'),
				'con_full'		=> $this->input->post('con_full'),
				'office_id'		=> $this->input->post('office_id'),
				"con_date"      => date("H:i:s - d/m/Y")
			);
			return $data;
		}
	}