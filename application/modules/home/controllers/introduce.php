<?php
class introduce extends MY_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->helper("url");
		$this->load->library("string");
		$this->load->model('mproduct');
		$this->load->model('mintroduce');
        $this->load->model("msupport");
        $this->load->model('mdoitac');
        $this->load->model('mnhanvien');
	}
	public function index(){
		
	    //=============== đối tác ============
        $doitacs = $this->mdoitac->getAll();
        if(!empty($doitacs))
        {
            foreach ($doitacs as $key => $value) {
                if(!empty($value['doitac_image'])){
                    $data['doitacs'][$key]['doitac_image'] = base_url().'uploads/doitac/'.$value['doitac_image'];
                }
                else
                {
                    $data['doitacs'][$key]['doitac_image'] = base_url().'public/images/no-images.jpg';   
                }
            }
        }
        //================ show introduce ================
        $data['title'] = 'Giới thiệu cửa hàng | Thông tin về chúng tôi | '.base_url();
        $data['gioithieu'] = $this->mintroduce->getIntroduce();
        $data['nv']        = $this->mnhanvien->getAll();
        //--------------- SYSTEM -----------------
        $data['menus'] = $this->mcategory->getAll();
        $data['config'] = $this->mconfig->getData();
        $data['support'] = $this->msupport->list_sup(2,0);
        //----------------------------------------

		$data['template'] 		= 'introduce/index';
		$this->load->view('layout', $data);
	}

}