<?php
    class Product extends MY_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->helper("url");
            $this->load->library("string");
            $this->load->model('mproduct');
            $this->load->model('msupport');
        }
        
        public function index()
        {
            $data['template'] = 'product/index';
            $this->load->view('layout', $data);
        }

        public function detail() {
            $params             = $this->uri->segment(2);
            $id                 = (int)end(explode("-", $params));
            $info               = $this->mproduct->getOnce($id);
            if(!empty($info))
            {
                if(!empty($info['pro_images']))
                {
                    @$images = unserialize($info['pro_images']);
                    $info['pro_images'] = base_url('uploads/products/'.$info['pro_folderimg'].'/'.$images[0]);
                    unset($images[0]);
                }
                else
                {
                    $info['pro_images'] = base_url('public/admin/images/no-images.jpg');
                }
            }
            $data['infos'] = $info;
            //=============== product new ============
            $data['pro_new'] = $this->mproduct->list_pro_new(3,0);
            if(!empty($data['pro_new']))
            {
                foreach ($data['pro_new'] as $key => $pro) {
                    if(!empty($pro['pro_images']))
                    {
                        @$images = unserialize($pro['pro_images']);
                        $data['pro_new'][$key]['pro_images'] = base_url().'uploads/products/thumb/'.$images[0];
                    }
                    else
                    {
                        $data['pro_new'][$key]['pro_images'] = base_url().'public/images/no-images.jpg';
                    }
                }
            }
            //============== product hot ==============
            $data['pro_hot'] = $this->mproduct->list_pro_hot(3,0);
            if(!empty($data['pro_hot']))
            {
                foreach ($data['pro_hot'] as $key => $pro) {
                    if(!empty($pro['pro_images']))
                    {
                        @$images = unserialize($pro['pro_images']);
                        $data['pro_hot'][$key]['pro_images'] = base_url().'uploads/products/'.$pro['pro_folderimg'].'/'.$images[0];
                    }
                    else
                    {
                        $data['pro_hot'][$key]['pro_images'] = base_url().'public/images/no-images.jpg';
                    }
                }
            }
            //============== product sale ==============
            $data['pro_bestsale'] = $this->mproduct->list_pro_bestsale(3,0);
            if(!empty($data['pro_bestsale']))
            {
                foreach ($data['pro_bestsale'] as $key => $pro) {
                    if(!empty($pro['pro_images']))
                    {
                        @$images = unserialize($pro['pro_images']);
                        $data['pro_bestsale'][$key]['pro_images'] = base_url().'uploads/products/'.$pro['pro_folderimg'].'/'.$images[0];
                    }
                    else
                    {
                        $data['pro_bestsale'][$key]['pro_images'] = base_url().'public/images/no-images.jpg';
                    }
                }
            }
            //--------------- SYSTEM -----------------
            $data['title'] = 'Chi tiết sản phẩm | Thông tin chi tiết sản phẩm | '.base_url();
            $data['menus'] = $this->mcategory->getAll();
            $data['config'] = $this->mconfig->getData();
            //----------------------------------------
            $data['template']   = 'product/detail';
            $this->load->view('layout', $data);
        }
    }
