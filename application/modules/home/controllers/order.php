<?php
	require("libraries/student.php");
	class order extends MY_Controller{
		public function __construct()
		{
			parent::__construct();
			$this->load->helper("url");
			$this->load->model("morder");
			$this->load->model("mcart");
			$this->load->model("msupport");
		}

		public function index()
		{
			//===================================

			if(isset($_POST['sendorder']))
			{
				$data['statusOrder'] = false;
				$this->_validateForm();
				$this->_messagesForm();
				if($this->form_validation->run())
				{
					$data = $this->getDataForm();
					if(isset($_SESSION['id']) && !empty($_SESSION['id']))
					{
						$data['user_id'] = $_SESSION['id'];
					}
					$this->morder->insertOrder($data);

					if (isset($_SESSION['cart']) && $_SESSION['cart']) {
	                    foreach ($_SESSION['cart'] as $cart) {
	                        $orderDetail = array(
	                            'pro_name' => $cart['name'],
	                            'image' => $cart['image'],
	                            'pro_qty' => $cart['qty'],
	                            'order_id' => $this->mcart->getLastOrderId()
	                        );
	                        $this->mcart->insertOrderDetail($orderDetail);
	                    }
	                    $data['statusOrder'] = true;
	                    mycart::deleteAll();
	                }
				}
			}

			//===================================
			//=================== System ========================
			$data['title'] = 'Đơn hàng sản phẩm đã lựa chọn | Đơn hàng sản phẩm | '.base_url();
			$data['menus'] = $this->mcategory->getAll();
			$data['config'] = $this->mconfig->getData();
			$data['support'] = $this->msupport->list_sup(2,0);

			$data['template'] = "order/index";
			$this->load->view("layout",$data);
		}

		public function getDataForm()
		{
			$data = array(
				'name'      => $this->input->post('con_name'),
				'email'     => $this->input->post('con_email'),
				'local'     => $this->input->post('con_local1'),
				're_local'  => $this->input->post('con_local2'),
				'phone'     => $this->input->post('con_phone1'),
				're_phone'     => $this->input->post('con_phone2'),
				'ship_method'  => $this->input->post('con_phuongthuc'),
				'pay_method'   => $this->input->post('con_thanhtoan'),
				'date'         => date("H:i:s - d/m/Y")
			);
			return $data;
		}

		public function _validateForm()
		{
			$this->form_validation->set_rules('con_name','Họ và Tên','required|min_length[4]');
			$this->form_validation->set_rules('con_email','Email','required|email');
			$this->form_validation->set_rules('con_local1','Địa chỉ nhận hàng','required');
			$this->form_validation->set_rules('con_phone1','Số điện thoại liên hệ','required|min_length[10]|max_length[14]');
			$this->form_validation->set_rules('con_thanhtoan','Phương thức thanh toán','required');
			$this->form_validation->set_rules('con_phuongthuc','Phương thức giao hàng','required');
		}

		public function _messagesForm()
		{
			$this->form_validation->set_message('required', '%s không được để trống');
			$this->form_validation->set_message('min_length', '%s không được ít hơn %d ký tự');
			$this->form_validation->set_message('max_length', '%s không được ít hơn %d ký tự');
			$this->form_validation->set_message('email', '%s không đúng định dạng');
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-validate">','</div>');
		}
	}