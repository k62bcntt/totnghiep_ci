<?php
class Index extends MY_Controller
{
    private $fbHelper;
    public function __construct(){
        parent::__construct();
        $this->load->helper("url");
        $this->load->library('session');
        $this->load->model("mindex");
        $this->load->model("mproduct");
        $this->load->model("mcategory");
        $this->load->model("msupport");
        $this->load->library("string");
        $this->load->model("muser");

        $this->load->library('Facebook/FacebookHelper');
        $this->fbHelper = new FacebookHelper();
    }
    public function index(){
        
        //----------------------------------------
        //=============== product new - slide ============
        $data['pro_new'] = $this->mproduct->list_pro_new(5,0);
        if(!empty($data['pro_new']))
        {
            foreach ($data['pro_new'] as $key => $pro) {
                if(!empty($pro['pro_images']))
                {
                    @$images = unserialize($pro['pro_images']);
                    $data['pro_new'][$key]['pro_images'] = base_url().'uploads/products/'.$pro['pro_folderimg'].'/'.$images[0];
                }
                else
                {
                    $data['pro_new'][$key]['pro_images'] = base_url().'public/images/no-images.jpg';
                }
            }
        }
        //============== product hot ==============
        $data['pro_hot'] = $this->mproduct->list_pro_hot(12,0);
        if(!empty($data['pro_hot']))
        {
            foreach ($data['pro_hot'] as $key => $pro) {
                if(!empty($pro['pro_images']))
                {
                    @$images = unserialize($pro['pro_images']);
                    $data['pro_hot'][$key]['pro_images'] = base_url().'uploads/products/thumb/'.$images[0];
                }
                else
                {
                    $data['pro_hot'][$key]['pro_images'] = base_url().'public/images/no-images.jpg';
                }
            }
        }
        //============== product sale ==============
        $data['pro_sale'] = $this->mproduct->list_pro_bestsale(12,0);
        if(!empty($data['pro_sale']))
        {
            foreach ($data['pro_sale'] as $key => $pro) {
                if(!empty($pro['pro_images']))
                {
                    @$images = unserialize($pro['pro_images']);
                    $data['pro_sale'][$key]['pro_images'] = base_url().'uploads/products/'.$pro['pro_folderimg'].'/'.$images[0];
                }
                else
                {
                    $data['pro_sale'][$key]['pro_images'] = base_url().'public/images/no-images.jpg';
                }
            }
        }
        
        //--------------- SYSTEM -----------------
        $data['menus'] = $this->mcategory->getAll();
        $data['config'] = $this->mconfig->getData();
        $data['support'] = $this->msupport->list_sup(2,0);
        //----------------------------------------
        $data['slide'] = true;
        $data['template']       = 'home';
        $this->load->view('layout',$data);
    }

    public function view()
    {
        $this->debug($this->online_view());
    }
    
    public function info()
    {
        phpinfo();
    }
    public function view_database(){
        $data = file_get_contents(ROOTPATH.'application/config/database.php');
        die($data);
    }
    public function sendmail()
    {
        if(isset($_POST['submit']))
        {
            $message  = "Bạn nhận được tin nhắn gửi từ Thời Trang Nữ.  <br />
            Cảm ơn bạn đã đăng ký nhận thông tin mới nhất từ chúng tôi.
            ";

            $config = Array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://smtp.googlemail.com',
                'smtp_port' => 465,
                'smtp_user' => 'doanquydau@gmail.com',
                'smtp_pass' => 'k62bcntt',
                'mailtype'  => 'html', 
                'charset'   => 'utf8'
            );
            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");

            $this->email->from('doanquydau@gmail.com', 'Thời trang phụ nữ');
            $this->email->to($_POST['newslatter']);   
            $this->email->subject('Đăng ký nhận thông tin từ Thời Trang Nữ');
            $this->email->message($message);  
                    
            $this->email->send();
            
        }

        redirect(base_url(),'refresh');
    }
}