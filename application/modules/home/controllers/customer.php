<?php
class customer extends MY_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->helper("url");
        $this->load->library("string");
    }
    public function index(){
        
        $data['template']       = 'customer/index';
        $this->load->view('layout', $data);
    }

    public function detail() {
        
        $data['template']   = 'customer/detail';
        $this->load->view('layout', $data);
    }
}