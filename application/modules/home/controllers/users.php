<?php
   class Users extends MY_Controller{
   	    private $fbHelper;
	    public function __construct(){
		   parent::__construct();
		   $this->load->helper("url");
		   $this->load->model("muser");
		   $this->load->model("msupport");
		   $this->load->library('session');
		   $this->load->model("morder");
		   $this->load->library("form_validation");

		    $this->load->library('Facebook/FacebookHelper');
        	$this->fbHelper = new FacebookHelper();
	    }
	    public function index(){}
	    public function login(){
		   	$data['title'] 		= "Đăng nhập và đăng ký";
			$data['online'] 	= $this->online();
			$data['access'] 	= $this->access();
			$this->write($data['access']);

			if(isset($_POST['btn_register']))
			{
				$this->_validateFormRegister();
				$this->_messagesForm();
				if($this->form_validation->run())
				{
					if($this->muser->check_user == true || $this->muser->check_email == true)
					{
						$data['message_error'] = 'Lỗi Tên đăng nhập hay Email đã bị trùng';
					}
					else
					{
						$dataForm = $this->getFormDataRegister();
						$this->muser->add($dataForm);
						$data['message_success'] = 'Đăng ký thành công ';
					}
				}
			}

			if(isset($_POST['btn_login']))
			{
				$this->_validateFormLogin();
				$this->_messagesForm();
				if($this->form_validation->run())
				{
					$username = $this->input->post('login_username');
					$password = $this->input->post('login_password');
					$login = $this->muser->login($username, md5($password));
					if(!empty($login))
					{
						$dateLastLogin = array(
							'cus_lastlogin' => date("H:i:s - d/m/Y")
						);
						$this->muser->update($dateLastLogin, $login['cus_id']);
						$_SESSION['id'] = $login['cus_id'];
						$_SESSION['username'] = $login['cus_name'];
						$_SESSION['email'] = $login['cus_email'];
						$_SESSION['name'] = $login['cus_fullname'];

						redirect(base_url());
					}
					else
					{
						$data['message_error'] = 'Tên đăng nhập không đúng';
					}
				}
			}
			
			//================ SYSTEM =================
			$data['title'] = 'Đăng nhập đăng ký thành viên | Thành viên của shop | '.base_url();
			$data['menus'] = $this->mcategory->getAll();
	        $data['config'] = $this->mconfig->getData();
	        //============================================
			$data['template'] = "user/login";
			$this->load->view("layout",$data);
	   }
	   
	   //========= get form data for Register =======
	   
	   public function fblogin()
	   {
			$url = $this->fbHelper->loginUrl();
			$_SESSION['session_fb'] = $this->fbHelper->getSession();
			redirect($url);
	   }

	   public function fb_login()
	   {
	   		$session_fb = $this->fbHelper->getSession();
	   		var_dump($session_fb);
	   	    if($session_fb)
	        {
	            $infor = $this->fbHelper->get_User();
	            
	            $emailfb = $this->muser->check_email($infor['email']);
	            if($emailfb)
	            {
	                $dateLastLogin = array(
	                    'cus_lastlogin' => date("H:i:s - d/m/Y"),
	                    'facebookToken' => $this->fbHelper->getFacebookAccessToken()
	                );
	                $this->muser->update($dateLastLogin, $infor['id']);
	                
	            }
	            else
	            {
	            	$fb_image = $this->fbHelper->fb_image();
	            	if(isset($infor['email']))
						$email= $infor['email'];
					else
						$email = "";
	                $insert = array(
	                    'cus_fullname' => $infor['fullname'],
	                    'cus_email'    => $email,
	                    'cus_phone'    => $infor['phone'],
	                    'cus_name'    => $email,
	                    'cus_password'    => uniqid(),
	                    'cus_status'    => '1',
	                    'cus_image' => $fb_image,
	                    'cus_date'      => date("H:i:s - d/m/Y"),
	                    'facebookToken' => $this->fbHelper->getFacebookAccessToken()
	                );

	                $this->User->add($insert);
	            }
	            $_SESSION['id'] = $infor['id'];
	            $_SESSION['username'] = $infor['email'];
	            $_SESSION['email'] = $infor['email'];
	            $_SESSION['name'] = $infor['fullname'];
	            var_dump($_SESSION);
	            redirect(base_url());
	        }

	   }

	   public function getFormDataRegister()
	   {
	   		$data = array(
	   			'cus_fullname' => htmlentities($this->input->post('fullname')),
	   			'cus_email'    => $this->input->post('email'),
	   			'cus_phone'    => $this->input->post('phone'),
	   			'cus_address'    => $this->input->post('local'),
	   			'cus_name'    => $this->input->post('username'),
	   			'cus_password'    => md5($this->input->post('password')),
	   			'cus_gender'    => $this->input->post('gender'),
	   			'cus_status'    => '1',
	   			'cus_date'      => date("H:i:s - d/m/Y")
	   		);
	   		return $data;
	   }

	   	public function _validateFormRegister()
		{
			$this->form_validation->set_rules('fullname','Họ và Tên','required|min_length[4]');
			$this->form_validation->set_rules('email','Email','required|email');
			$this->form_validation->set_rules('username','Tên đăng nhập','required|min_length[6]|max_length[12]');
			$this->form_validation->set_rules('phone','Số điện thoại liên hệ','required|min_length[10]|max_length[14]');
			$this->form_validation->set_rules('password','Mật khẩu','required|min_length[6]|max_length[12]');
			$this->form_validation->set_rules('gender','Giới tính','required');
			$this->form_validation->set_rules('agress','Tích chọn ô','required');
		}

		public function _messagesForm()
		{
			$this->form_validation->set_message('required', '%s không được để trống');
			$this->form_validation->set_message('min_length', '%s không được ít hơn %d ký tự');
			$this->form_validation->set_message('max_length', '%s không được ít hơn %d ký tự');
			$this->form_validation->set_message('email', '%s không đúng định dạng');
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-validate">','</div>');
		}

	   //========= get form data for login =======

	   public function _validateFormLogin()
		{
			$this->form_validation->set_rules('login_username','Tên đăng nhập','required|min_length[6]|max_length[12]');
			$this->form_validation->set_rules('login_password','Mật khẩu','required|min_length[6]|max_length[12]');
		}

	   //===========================================
	   public function logout(){
		    unset($_SESSION['id']);
			unset($_SESSION['username']);
			unset($_SESSION['email']);
			unset($_SESSION['name']);
		    redirect(base_url());
	   }
	   public function verify(){
		   if($_POST == NULL){die();}
		   $name = $this->fillter($_POST['name']);
		   $pass = $this->fillter(md5($_POST['pass']));
		   $checklog = $this->muser->login($name,$pass);
		   if($checklog == FALSE){
			   echo "Tài khoản đăng nhập sai";
		   }else{
			   $_SESSION['userid'] = $checklog['cus_id'];
			   $_SESSION['username'] = $checklog['cus_name'];
			   $_SESSION['userlevel'] = $checklog['cus_level'];
			   $_SESSION['userlevel'] = $checklog['cus_level'];
			   $_SESSION['userlevel'] = $checklog['cus_level'];
			   $db 	 = array(
					"cus_lastlogin" => date("H:i:s - d/m/Y"),
					"cus_ip" 		 => getenv("REMOTE_ADDR")
				);
				$this->muser->update($db,$checklog['cus_id']);
			   echo "bachnx";
		   }
	   }
	   public function ajax(){
		   if($_POST == NULL){die();}
		   $code = $_SESSION['security_code'];
		   $name = $this->fillter($_POST['name']);
		   $pass = $this->fillter($_POST['pass']);
		   $fullname = $this->fillter($_POST['fullname']);
		   $email = $this->fillter($_POST['email']);
		   $phone = $this->fillter($_POST['phone']);
		   $gender = $this->fillter($_POST['gender']);
		   $captcha = $this->fillter($_POST['captcha']);
		   $data = array("cus_name"=>$name,"cus_password"=>md5($pass),"cus_fullname"=>$fullname,"cus_gender"=>$gender,"cus_phone"=>$phone,"cus_email"=>$email,"cus_level"=>"2","cus_status"=>"1","cus_date" 	=> date("H:i:s - d/m/Y"),"cus_day" 	=> date("d-m-Y"));
		   if($code != $captcha){
			   echo "Mã captcha chưa đúng";
		   }else{
			   $rulem = "/^[a-zA-Z]{1}[a-zA-Z0-9._]{3,25}\@[a-zA-Z0-9]{3,}\.[a-zA-Z.]{2,8}$/";
			   $rulep =  "/^[0]{1}[0-9]{8,12}$/";
			   if(!preg_match($rulem,$email)){
				   echo  "Email không hợp lệ"; die();
			   }else{
				   $checkname = $this->muser->getdata(array("cus_name"=>$name));
				   $checkemail = $this->muser->getdata(array("cus_email"=>$email));
				   if($checkname > 0){
					   echo "Tên đăng nhập đã tồn tại";
				   }elseif($checkemail > 0){
					   echo "Email này đã tồn tại";
				   }else{
					   $this->muser->add($data);
					   echo "bachnx";
				   }
			   }
		   }
	   }
	   public function manage(){
		   	$data['title'] 		= "Quản lý tài khoản";
			$id = $_SESSION['userid'];
			$data['online'] 	= $this->online();
			$data['access'] 	= $this->access();
			$this->write($data['access']);
			$data['pro_view'] 		= $this->mindex->list_pro_view();
			$data['listall'] 	= $this->mindex->listall();
			$get_setup 			= $this->mindex->get_setup();
			$data['pro_hot'] 	= $this->mindex->list_pro_hot($get_setup['set_pro_hot']);
			$data['info']		= $this->muser->getuser($id);
			//$this->debug($data['info']);
			$data['template'] = "user/user_info";
			$this->load->view("user/layout",$data);
	   }
	   public function save(){
		   	$data['title'] 		= "Sản phẩm yêu thích";
			$id = $_SESSION['userid'];
			$data['online'] 	= $this->online();
			$data['access'] 	= $this->access();
			$this->write($data['access']);
			$data['pro_view'] 		= $this->mindex->list_pro_view();
			$data['listall'] 	= $this->mindex->listall();
			$get_setup 			= $this->mindex->get_setup();
			$data['pro_hot'] 	= $this->mindex->list_pro_hot($get_setup['set_pro_hot']);
			$data['list'] = $this->muser->user_like_content($id);
			$data['template'] = "user/user_save";
			$this->load->view("user/layout",$data);
	   }
	   public function remove_like(){
		   $pro_id = $_POST['pro_id'];
		   $this->muser->del_like_content($pro_id);
	   }
	   public function update_info(){
		   	$data['title'] 		= "Cập nhật thông tin";
			$id = $_SESSION['id'];
			
			$data['online'] 	= $this->online();
			$data['access'] 	= $this->access();
			$this->write($data['access']);
			$data['template'] = "user/user_update";
			if($this->input->post("update_info")){
				$this->_validateFormUpdateInfo();
				$this->_messagesForm();
				if($this->form_validation->run())
				{
					$val = array(
						"cus_fullname" => $this->fillter($this->input->post("fullname")),
						"cus_address" => $this->fillter($this->input->post("address")),
						"cus_phone" => $this->fillter($this->input->post("phone")),
						"cus_gender" => $this->fillter($this->input->post("gender")),
						"cus_email" => $this->fillter($this->input->post("email"))
					);
					$image = $this->uploadAnhdaidien();
					if(!empty($image))
					{
						$val['cus_image'] = $image;
					}
					// var_dump($val); die;
					$result = $this->muser->update($val,$id);
					if($result){
						$data['error'] = 'Cập nhật thông tin "KHÔNG" thành công !';
					}
					else
					{
						$data['success'] = "Cập nhật thông tin thành công !";
					}
				}
			}
			if($this->input->post("update_pass")){
				$this->form_validation->set_rules('password','Mật khẩu','required|min_length[6]|max_length[12]');
				$this->form_validation->set_rules('repass','Nhập lại mật khẩu','required|min_length[6]|max_length[12]|matches["password"]');
				$this->form_validation->set_message('matches','%s phải khớp với %s');
				$this->_messagesForm();
				if($this->form_validation->run())
				{
					if($this->muser->check_pass($this->input->post("oldpassword"), $id))
					{
						if($this->input->post("password") != NULL){
							$val = array();
							if($this->input->post("password") != $this->input->post("repass")){
								$data['error'] = "Nhập lại mật khẩu không đúng !";
							}else{
							  	$val['cus_password'] = $this->fillter(md5($this->input->post("password")));
							  	$data['success'] = "Cập nhật mật khẩu thành công !";
							}
							$this->muser->update($val,$id);
						}
						
					}
					else
					{
						$data['error'] = 'Mật khẩu cũ không đúng !';
					}
				}
			}
			$data['info']		= $this->muser->getuser($id);
			if(empty($data['info']['cus_image']))
			{
				$data['info']['cus_image'] = base_url('public/admin/images/no-images.jpg');
			}
			$order_id = $this->morder->getOnceOrder($id);
			$data['my_order'] = $this->morder->getOnceCart($order_id['id']);
			if(!empty($data['my_order']))
			{
				foreach ($data['my_order'] as $key => $value) {
					$data['my_order'][$key]['local_phone'] = 'Địa chỉ: '.$order_id['local'].'<br />Số điện thoại: '.$order_id['phone'];
				}
			}
			//--------------- SYSTEM -----------------
			$data['title'] = 'Thông tin cá nhân | Thông tin người dùng | '.base_url();
	        $data['menus'] = $this->mcategory->getAll();
	        $data['config'] = $this->mconfig->getData();
	        $data['support'] = $this->msupport->list_sup(2,0);
	        //----------------------------------------
			$this->load->view("layout",$data);	
	   }
	   public function _validateFormUpdateInfo()
		{
			$this->form_validation->set_rules('fullname','Họ và Tên','required|min_length[4]');
			$this->form_validation->set_rules('email','Email','required|email');
			$this->form_validation->set_rules('phone','Số điện thoại liên hệ','required|min_length[10]|max_length[14]');
			$this->form_validation->set_rules('gender','Giới tính','required');
		}
		public function uploadAnhdaidien()
		{
			$flag = "";
	        if($_FILES['anhdaidien']['name'] != "") {
	            $config['upload_path'] = './uploads/thanhvien/';
	            $config['allowed_types'] = 'gif|jpg|png|jpeg';
	            $config['max_size'] = '1000000';
	            // $config['max_width'] = '10000';
	            // $config['max_height'] = '10000';
	            $config['encrypt_name'] = TRUE;
	            $this->load->library('upload', $config);
	            if(!$this->upload->do_upload('anhdaidien')){
	               $errors = $this->upload->display_errors("<p></p>");
	               if ($errors) {
	                  $flag = false;
	               }
	            } else {
	                $file_info = $this->upload->data();
	                $config = array(
	                    "source_image" =>"./uploads/thanhvien/". $file_info['file_name'],
	                    "create_thumb"=> true,
	                    "maintain_ration" =>true,
	                    "width" =>220,
	                    "height" =>200,
	                );

	                $this->load->library("image_lib",$config);

	                if(!$this->image_lib->resize()) {
	                    $data['errors'] = $this->image_lib->display_errors();
	                }else{
	                    $this->image_lib->resize();
	                }
	                $flag = base_url().'uploads/thanhvien/'. $file_info['file_name'];
	            }
	        }
	        return $flag;
		}
	   //=================================================================
	   public function user_like_content(){
		   if(isset($_SESSION['userid'])){
				$pro_id = $_POST['pro_id'];
				$uid = $_SESSION['userid'];
				$data = $this->muser->check_like_content($uid,$pro_id);
				if($data == FALSE){
					echo 2;
				}else{
					$data = array(
						"fa_userid" 	=> $uid,
						"pro_id"	=> $pro_id
					);
					$this->muser->insert($data);
				}
			}else{
				echo 1;
			}
	   }
	   public function forgot(){
		   $data['title'] = "Quên mật khẩu";
		   $data['access'] = $this->access();
		   $data['online'] = $this->online();
		   if($this->input->post("ok")){
			   $this->form_validation->set_rules("email","Email","required|min_length[5]|valid_emails");
			   $this->form_validation->set_rules("captcha","Mã xác nhận","required|min_length[4]");
			   if($this->form_validation->run() == FALSE){
				  $this->load->view("user/forgot/layout",$data); 
			   }else{
				   $ses = $_SESSION['security_code'];
				   $code = $this->input->post("captcha");
				   $email = $this->input->post("email");
				   if($code != $ses){
					   $data['error'] = "Mã xác nhận không chính xác";
					   $this->load->view("user/forgot/layout",$data);
				   }else{
					   if($this->muser->check_email($email) == TRUE){
						  	$data['error'] = "Không tìm thấy email của bạn trong cơ sở dữ liệu";
					   		$this->load->view("user/forgot/layout",$data); 
					   }else{
						   	$rand = rand(0,999);
							$pass = md5($rand);
							$profile = $this->muser->get_forgot($email);
						    $message  = "Chúng tôi nhận được yêu cầu lấy lại mật khẩu của bạn từ http://vinhphucit.org/ <br />";
							$message .= "Tài khoản của bạn : ".$profile['username']. "<br />";
							$message .= "Mật khẩu mới của bạn : ".$rand;
							$mail = array(
	                            "to_receiver"   => $email,
	                            "message"       => $message,
	                        	);	
							$pa = array("password"=>"$pass");			                
							$this->muser->forgot($email,$pa);
							$this->load->library("my_email");
				            $this->my_email->config($mail);
				            $this->my_email->sendmail();
							$data['error'] = "Một tin nhắn đã gửi đến email của bạn,vui lòng check email để lấy lại mật khẩu !";
							$this->load->view("user/forgot/layout",$data);
						}
				   }
			   }
		   }else{
		   		$this->load->view("user/forgot/layout",$data);
		   }
	   }
	   public function sendmail($data){
	   }
	   public function createThumbnail($fileName,$width,$height){
			$this->load->library('image_lib');
			//$this->load->helper('thumbnail_helper');
			$config['image_library'] = 'gd2';
			$config['source_image'] = 'uploads/users/'.$fileName;
			$config['new_image'] = 'uploads/users/thumb/'.$fileName;
			$config['create_thumb'] = TRUE;
			$config['maintain_ratio'] = TRUE;
			$config['thumb_marker'] = FALSE;
			$config['width'] = $width;
			$config['height'] = $height;
			$this->image_lib->initialize($config); 
			$this->image_lib->resize();
			$this->image_lib->clear();
		}
   }